// get defaults assetExts array
const defaultAssetExts = require("metro-config/src/defaults/defaults").assetExts;

module.exports = {
    transformer: {
        getTransformOptions: async () => ({
            transform: {
                experimentalImportSupport: false,
                inlineRequires: false,
            },
        }),
    },
    resolver: {
        assetExts: [
            'svg',
            'jpg',
            'png',
            'ttf',
            'md'
        ]
    }
};