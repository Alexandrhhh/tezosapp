import React, { Component } from 'react';
import { View, StyleSheet, Image, ScrollView, TextInput, TouchableOpacity, Platform, Dimensions, AsyncStorage} from 'react-native';
import { Container, Content, Text, Spinner, Button, Toast} from 'native-base';
import Head from '../Header';
import {SvgXml} from 'react-native-svg';
import { normalize } from '../normalize';
import * as WebBrowser from 'expo-web-browser';

export default class Homescreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          bakers: [],
          baker_update: false,
          tezos_update: false,
          sleep_bakers: [],
          loading: false,
          tezos: {
              public_bakers: 0
          },
          newses: [],
          assetValue: 0,
          authorized: false,
          login: '',
          address: '',
          password: '',
          showToast: false,
          token: '',
          token_type: '',

        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    bakersUpdate() {
        this.setState({ baker_update: false})
        fetch("https://api.tezos-nodes.com/v1/bakers")
        .then(response => response.json())
        .then(json => {
            this.setState({ bakers: json })
            this.setState({ baker_update: true})
        })
    }

    async openUrl(uri) {
        await WebBrowser.openBrowserAsync(uri);
    }

    onChanged(text){
        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            }
            else {
                // your call back function
                alert("please enter numbers only");
            }
        }
        this.setState({ assetValue: newText });
    }

    tezosUpdate() {
        this.setState({ tezos_update: false});
        fetch("https://api.tezos-nodes.com/v1/tezos")
        .then(response => response.json())
        .then(json => {
            this.setState({ tezos: json });
            this.setState({ loading: true});
            this.setState({ tezos_update: true});
        })
    }

    number_format(number, decimals, dec_point, thousands_sep) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          var k = Math.pow(10, prec);
          return '' + (Math.round(n * k) / k)
            .toFixed(prec);
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '')
        .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
          .join('0');
      }
      return s.join(dec);
    }

    login() {
        let user = {
            email: this.state.login,
            password: this.state.password
        }
        fetch('https://api.tezos-nodes.com/api/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            },
            body: 'email=' + user.email + '&password=' + user.password
        })
        .then(response => response.json())
        .then(json => {
            if(json.error) {
                Toast.show({
                  text: 'The username or password is incorrect',
                  textStyle: {textAlign: 'center'},
                  type: 'danger',
                  duration: 2000
                })
            } else {
                Toast.show({
                  text: 'Authorization was successful!',
                  textStyle: {textAlign: 'center'},
                  type: 'success',
                  duration: 2000
                })
                this.setState({authorized: true, token: json.access_token, token_type: json.token_type})
                AsyncStorage.setItem('token', json.access_token);
                AsyncStorage.setItem('token_type', json.token_type);
            }
        })
    }

    logout() {
        AsyncStorage.removeItem('token')
        AsyncStorage.removeItem('token_type')
        this.setState({token: '', token_type: '', authorized: false})
    }

    async authorized() {
        await AsyncStorage.getItem('token')
        .then((token) => {
            if(token !== null) {
                this.setState({authorized: true, token: token})
            } else {
                this.setState({authorized: false})
            }
        })
        await AsyncStorage.getItem('token_type')
        .then((token_type) => {
            if(token_type !== null) {
                this.setState({token_type: token_type})
            }
        })
        await fetch("https://api.tezos-nodes.com/v1/news")
        .then(response => response.json())
        .then(json => {
            this.setState({ newses: json })
            let arr = Array();
            json.map((news) => {
                arr.push(news.id)
            })
            AsyncStorage.setItem('news', JSON.stringify(arr));
        })
    }

    componentDidMount() {
       fetch("https://api.tezos-nodes.com/v1/bakers")
       .then(response => response.json())
       .then(json => {
           this.setState({ bakers: json })
           this.setState({ baker_update: true})
       })

       fetch("https://api.tezos-nodes.com/v1/tezos")
       .then(response => response.json())
       .then(json => {
           this.setState({ tezos: json });
           this.setState({ loading: true});
           this.setState({ tezos_update: true});
       })

       fetch("https://api.tezos-nodes.com/v1/sleep_bakers")
       .then(response => response.json())
       .then(json => this.setState({ sleep_bakers: json }))
       fetch("https://api.tezos-nodes.com/v1/news")
       .then(response => response.json())
       .then(json => {
           this.setState({ newses: json })
           let arr = Array();
           json.map((news) => {
               arr.push(news.id)
           })
           AsyncStorage.setItem('news', JSON.stringify(arr));
       })

       this.authorized();

       this._unsubscribe = this.navigation.addListener('focus', () => {
         this.authorized();
       });
   }

   componentWillUnmount() {
       this._unsubscribe();
   }

  render() {
     let { bakers, sleep_bakers, newses, assetValue } = this.state;
     const {tezos, loading, address} = this.state;
     bakers = bakers.filter(baker => baker.rank < 11);
     sleep_bakers = sleep_bakers.filter(baker => baker.rank < 5);
     const svgArrow = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.78279 5.99274L1.72295 0.207681C1.5828 0.0737718 1.3957 0 1.1962 0C0.996706 0 0.809609 0.0737718 0.669453 0.207681L0.223188 0.633613C-0.0671998 0.911155 -0.0671998 1.36224 0.223188 1.63936L5.31178 6.4973L0.217542 11.3606C0.0773851 11.4945 0 11.6731 0 11.8634C0 12.054 0.0773851 12.2325 0.217542 12.3665L0.663807 12.7923C0.804074 12.9262 0.99106 13 1.19056 13C1.39005 13 1.57715 12.9262 1.71731 12.7923L7.78279 7.00198C7.92328 6.86764 8.00044 6.68829 8 6.49762C8.00044 6.30622 7.92328 6.12697 7.78279 5.99274Z" fill="#319DFE"/></svg>';
     const svgAccount = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="25" height="25" viewBox="0 0 25 25" fill="none"><path d="M0 12.5C0 5.59139 5.59063 0 12.5 0C19.4086 0 25 5.59063 25 12.5C25 19.3474 19.4561 25 12.5 25C5.57251 25 0 19.3789 0 12.5ZM12.5 1.46484C6.41518 1.46484 1.46484 6.41518 1.46484 12.5C1.46484 14.7259 2.12688 16.8653 3.35464 18.6775C8.28476 13.3743 16.7063 13.3648 21.6454 18.6775C22.8731 16.8653 23.5352 14.7259 23.5352 12.5C23.5352 6.41518 18.5848 1.46484 12.5 1.46484ZM20.7317 19.8486C16.3483 14.9319 8.65059 14.933 4.26846 19.8486C8.66661 24.7692 16.3317 24.7707 20.7317 19.8486Z" fill="#3E4257"/><path d="M12.5 13.2324C10.0769 13.2324 8.10547 11.261 8.10547 8.83789V7.37305C8.10547 4.94995 10.0769 2.97852 12.5 2.97852C14.9231 2.97852 16.8945 4.94995 16.8945 7.37305V8.83789C16.8945 11.261 14.9231 13.2324 12.5 13.2324ZM15.4297 7.37305C15.4297 5.75752 14.1155 4.44336 12.5 4.44336C10.8845 4.44336 9.57031 5.75752 9.57031 7.37305V8.83789C9.57031 10.4534 10.8845 11.7676 12.5 11.7676C14.1155 11.7676 15.4297 10.4534 15.4297 8.83789V7.37305Z" fill="#3E4257"/></svg>';
     const svgPassword = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="22" height="25" viewBox="0 0 22 25" fill="none"><path d="M16.5039 17.8711C17.0432 17.8711 17.4805 17.4339 17.4805 16.8945C17.4805 16.3552 17.0432 15.918 16.5039 15.918C15.9646 15.918 15.5273 16.3552 15.5273 16.8945C15.5273 17.4339 15.9646 17.8711 16.5039 17.8711Z" fill="#3E4257"/><path d="M16.6951 9.17969V9.27969H16.7951H17.9688C20.0674 9.27969 21.775 10.9873 21.775 13.0859V16.6992C21.775 17.1833 21.3826 17.5758 20.8984 17.5758C20.4143 17.5758 20.0219 17.1833 20.0219 16.6992V13.0859C20.0219 11.9538 19.1009 11.0328 17.9688 11.0328H3.90625C2.77407 11.0328 1.85312 11.9538 1.85312 13.0859V21.0938C1.85312 22.2259 2.77407 23.1469 3.90625 23.1469H17.9688C19.1009 23.1469 20.0219 22.2259 20.0219 21.0938C20.0219 20.6096 20.4143 20.2172 20.8984 20.2172C21.3826 20.2172 21.775 20.6096 21.775 21.0938C21.775 23.1924 20.0674 24.9 17.9688 24.9H3.90625C1.80757 24.9 0.1 23.1924 0.1 21.0938V13.0859C0.1 10.9873 1.80757 9.27969 3.90625 9.27969H5.07632H5.17632V9.17969V5.73564C5.17632 2.63023 7.75804 0.1 10.9357 0.1C14.1133 0.1 16.6951 2.63023 16.6951 5.73564V9.17969ZM14.8419 9.27969H14.9419V9.17969V5.73564C14.9419 3.59171 13.1418 1.85312 10.9357 1.85312C8.72962 1.85312 6.92944 3.59171 6.92944 5.73564V9.17969V9.27969H7.02944H14.8419Z" fill="#3E4257" stroke="#090A11" stroke-width="0.2"/><path d="M9.13086 17.8711C9.6702 17.8711 10.1074 17.4339 10.1074 16.8945C10.1074 16.3552 9.6702 15.918 9.13086 15.918C8.59152 15.918 8.1543 16.3552 8.1543 16.8945C8.1543 17.4339 8.59152 17.8711 9.13086 17.8711Z" fill="#3E4257"/><path d="M5.46875 17.8711C6.00809 17.8711 6.44531 17.4339 6.44531 16.8945C6.44531 16.3552 6.00809 15.918 5.46875 15.918C4.92941 15.918 4.49219 16.3552 4.49219 16.8945C4.49219 17.4339 4.92941 17.8711 5.46875 17.8711Z" fill="#3E4257"/><path d="M12.793 17.8711C13.3323 17.8711 13.7695 17.4339 13.7695 16.8945C13.7695 16.3552 13.3323 15.918 12.793 15.918C12.2536 15.918 11.8164 16.3552 11.8164 16.8945C11.8164 17.4339 12.2536 17.8711 12.793 17.8711Z" fill="#3E4257"/></svg>';
     const svgCycle = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="51" height="48" viewBox="0 0 51 48" fill="none"><path d="M39.6318 44.8945C44.8713 40.89 48.3111 34.629 48.4737 27.5232C48.6132 21.4253 46.3133 15.8386 42.4659 11.6969C38.4751 7.40072 32.8194 4.65924 26.4941 4.51455C14.071 4.23037 3.76963 14.071 3.48545 26.4941C3.31451 33.9667 6.8068 40.6716 12.3209 44.8906" stroke="#30DD36" stroke-dashoffset="0" stroke-width="6" stroke-linejoin="bevel" stroke-dasharray="110" /><path d="M39.6318 44.8945C44.8713 40.89 48.3111 34.629 48.4737 27.5232C48.6132 21.4253 46.3133 15.8386 42.4659 11.6969C38.4751 7.40072 32.8194 4.65924 26.4941 4.51455C14.071 4.23037 3.76963 14.071 3.48545 26.4941C3.31451 33.9667 6.8068 40.6716 12.3209 44.8906" stroke="#C4DFFF" stroke-dashoffset="' + tezos.cycle_percent*1.07 + '" stroke-width="6" stroke-linejoin="bevel" stroke-dasharray="110" /><path d="M39.6318 44.8945C44.8713 40.89 48.3111 34.629 48.4737 27.5232C48.6132 21.4253 46.3133 15.8386 42.4659 11.6969C38.4751 7.40072 32.8194 4.65924 26.4941 4.51455C14.071 4.23037 3.76963 14.071 3.48545 26.4941C3.31451 33.9667 6.8068 40.6716 12.3209 44.8906" stroke="#1E2233" stroke-width="7" stroke-linejoin="bevel" stroke-dasharray="2.5 1.5" /></svg>';
     const svgUpdate = '<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14 6.12497V0L11.9425 2.05623C10.6773 0.787498 8.93058 0 6.99561 0C3.1257 0 0 3.13248 0 7C0 10.8675 3.1257 14 6.99561 14C10.257 14 12.9887 11.7688 13.7636 8.75001H11.9425C11.2202 10.7888 9.28518 12.25 6.99565 12.25C4.09321 12.25 1.74237 9.90062 1.74237 7C1.74237 4.09938 4.09321 1.75001 6.99565 1.75001C8.44469 1.75001 9.74488 2.35376 10.6948 3.30315L7.87118 6.12502H14V6.12497Z" fill="#737B9C"/></svg>';
     const svgPro = '<svg width="41" height="26" viewBox="0 0 41 26" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="0.5" y="0.5" width="40" height="25" rx="4.5" stroke="#319DFE"/><path d="M12.04 14.696V19H10.68V7.8C11.128 7.8 11.6027 7.79467 12.104 7.784C12.6053 7.77333 13.1067 7.768 13.608 7.768C15.08 7.768 16.136 8.09867 16.776 8.76C17.4267 9.41067 17.752 10.2373 17.752 11.24C17.752 12.2107 17.4 13.0427 16.696 13.736C16.0133 14.4187 15.0213 14.76 13.72 14.76C13.0373 14.76 12.4773 14.7387 12.04 14.696ZM13.736 9.048C13.544 9.048 12.9787 9.05867 12.04 9.08V13.4C12.712 13.4533 13.256 13.48 13.672 13.48C14.4933 13.48 15.1387 13.2827 15.608 12.888C16.0773 12.4827 16.312 11.928 16.312 11.224C16.312 10.5307 16.0933 9.99733 15.656 9.624C15.2293 9.24 14.5893 9.048 13.736 9.048ZM20.4476 19H19.1676V11.8H20.4476V12.648C20.8743 11.9973 21.477 11.672 22.2556 11.672C22.757 11.672 23.1303 11.7627 23.3756 11.944L22.9276 13.144C22.6183 12.9627 22.293 12.872 21.9516 12.872C21.301 12.872 20.7996 13.2133 20.4476 13.896V19ZM25.1811 18.104C24.4771 17.3787 24.1251 16.4773 24.1251 15.4C24.1251 14.312 24.4771 13.416 25.1811 12.712C25.8958 11.9973 26.8238 11.64 27.9651 11.64C29.1065 11.64 30.0291 11.9973 30.7331 12.712C31.4478 13.416 31.8051 14.312 31.8051 15.4C31.8051 16.488 31.4478 17.3893 30.7331 18.104C30.0291 18.808 29.1065 19.16 27.9651 19.16C26.8345 19.16 25.9065 18.808 25.1811 18.104ZM26.1891 13.56C25.7198 14.0293 25.4851 14.6427 25.4851 15.4C25.4851 16.1573 25.7145 16.7707 26.1731 17.24C26.6425 17.72 27.2398 17.96 27.9651 17.96C28.6905 17.96 29.2825 17.7253 29.7411 17.256C30.2105 16.776 30.4451 16.1573 30.4451 15.4C30.4451 14.664 30.2105 14.0507 29.7411 13.56C29.2825 13.08 28.6905 12.84 27.9651 12.84C27.2398 12.84 26.6478 13.08 26.1891 13.56Z" fill="#319DFE"/></svg>';

     if(tezos.percent_change_24h > 0) {
         var svgChange = '<svg width="6" height="4" viewBox="0 0 6 4" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6 4H0L3 0L6 4Z" fill="#30DD36"/></svg>';
         var changeColor = '#30DD36';
     } else {
          var svgChange = '<svg width="6" height="4" viewBox="0 0 6 4" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 0L6 0L3 4L0 0Z" fill="#F91525"/></svg>';
          var changeColor = '#F91525';
     }
      if(tezos.percent_change_btc_24h > 0) {
          var svgBtcChange = '<svg width="6" height="4" viewBox="0 0 6 4" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6 4H0L3 0L6 4Z" fill="#30DD36"/></svg>';
          var changeBtcColor = '#30DD36';
      } else {
          var svgBtcChange = '<svg width="6" height="4" viewBox="0 0 6 4" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 0L6 0L3 4L0 0Z" fill="#F91525"/></svg>';
          var changeBtcColor = '#F91525';
      }
     if(this.state.loading) {
         return (
             <Container>
                 <Head navigation={this.navigation} route={this.route} />
                 <ScrollView style={{backgroundColor: '#1E2233'}}>
                     <Content style={{padding: 10, paddingTop: 0}}>
                         <TouchableOpacity onPress={() => this.navigation.navigate('News')}>
                             <View style={styles.header}>
                                 <Text style={styles.h2}>
                                     News
                                 </Text>
                                 <SvgXml xml={svgArrow} width="8" height="13" />
                             </View>
                         </TouchableOpacity>
                         {newses.map((news, i) => (
                             i < 1 ?
                             <TouchableOpacity activeOpacity = { .8 } key={news.id} onPress={() => this.navigation.navigate('PersonalNews', {id: news.id})}>
                                 <View style={styles.block_news}>
                                     <View style={{width: 80, height: 80, borderRadius: 12, overflow: 'hidden'}}>
                                         <Image source={{uri: news.image}}  style={{resizeMode:'cover',width:null,height:null, flex:1}}/>
                                     </View>
                                     <Text style={styles.block_news_text}>{news.title}</Text>
                                 </View>
                             </TouchableOpacity>
                             :
                             <View key={i}></View>
                         ))}
                         <View style={[styles.header, {justifyContent: 'space-between'}]}>
                             <TouchableOpacity onPress={() => this.navigation.navigate('Rating')}>
                                 <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                     <Text style={styles.h2}>
                                         Rating Bakers
                                     </Text>
                                     <SvgXml xml={svgArrow} width="8" height="13" />
                                 </View>
                             </TouchableOpacity>
                             <TouchableOpacity onPress={() => this.bakersUpdate()}>
                                 <SvgXml xml={svgUpdate} width="14" height="14" style={{padding: 7, marginRight: 10}} />
                             </TouchableOpacity>
                         </View>
                         <View>
                             {!this.state.baker_update ?
                                 <Spinner color='#319DFE' />
                                 :
                                 <ScrollView>
                                     <View style={{flex: 1, flexDirection: 'row', flexWrap: 'nowrap', borderRadius: 20, overflow: 'hidden'}}>
                                         <View style={{maxWidth: '55%', overflow: 'hidden'}}>
                                             <View style={styles.theadtr}>
                                                 <Text style={[styles.th, {width: '100%'}]}># Baker</Text>
                                             </View>
                                             {bakers.map((baker) => (
                                                 <TouchableOpacity key={baker.rank} activeOpacity={.8} onPress={() => this.navigation.navigate('Baker', {tz: baker.address})}>
                                                     <View style={styles.tbodytr}>
                                                         <View style={[styles.td, {width: '100%', flex: 1, flexDirection: 'row', alignItems: 'center'}]}>
                                                             {baker.pro_status ?
                                                                 <Image source={require('../../assets/icons/pro.png')} width="41" height="26" style={{marginRight: 5, marginLeft: -8}}/>
                                                                 :
                                                                 <Text style={{color: '#EAECF2', fontFamily: 'Circe', fontSize: normalize(16), marginRight: 10}}>{baker.rank}</Text>
                                                             }
                                                             <View style={{width: 24, marginRight: 5}}><Image source={{uri: baker.logo_min}} style={{resizeMode:'contain',width:null,height:null, flex:1}}/></View>
                                                             <Text style={styles.table_text}>{baker.name}</Text>
                                                         </View>
                                                     </View>
                                                 </TouchableOpacity>
                                             ))}
                                         </View>
                                         <View style={{maxWidth: '45%', borderLeftWidth: 1, borderLeftColor: '#30333F'}}>
                                             <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                                 <View>
                                                     <View style={styles.theadtr}>
                                                         <Text style={[styles.th, {width: 60}]}>Yield</Text>
                                                         <Text style={[styles.th, {width: 83}]}>Efficiency</Text>
                                                         <Text style={[styles.th, {width: 83}]}>Last 10</Text>
                                                         <Text style={[styles.th, {width: 80}]}>Fee</Text>
                                                         <Text style={[styles.th, {width: 120}]}>Free space</Text>
                                                         <Text style={[styles.th, {width: 120}]}>Total points</Text>
                                                     </View>
                                                     {bakers.map((baker) => (
                                                             <View style={styles.tbodytr} key={baker.rank}>
                                                                 <Text style={[styles.td, {width: 60}]}>{baker.yield.toFixed(2)}%</Text>
                                                                 <View style={[styles.td, {width: 83}]}>
                                                                     <View style={[styles.tint, baker.efficiency > 96 ? styles.green : baker.efficiency > 90 ? styles.yellow : styles.red]}>
                                                                         <Text style={[styles.tint_text, {textAlign: 'center'}, , baker.efficiency > 96 ? styles.green : baker.efficiency > 90 ? styles.yellow : styles.red]}>{baker.efficiency}%</Text>
                                                                     </View>
                                                                 </View>
                                                                 <View style={[styles.td, {width: 83}]}>
                                                                     <View style={[styles.tint, baker.efficiency_last10cycle > 96 ? styles.green : baker.efficiency_last10cycle > 90 ? styles.yellow : styles.red]}>
                                                                         <Text style={[styles.tint_text, {textAlign: 'center', flexWrap: 'nowrap'}, , baker.efficiency_last10cycle > 96 ? styles.green : baker.efficiency_last10cycle > 90 ? styles.yellow : styles.red]}> {baker.efficiency_last10cycle}%</Text>
                                                                     </View>
                                                                 </View>
                                                                 <Text style={[styles.td, {width: 80}]}>{(baker.fee*100).toFixed(2)}%</Text>
                                                                 <Text style={[styles.td, {width: 120}, (baker.deletation_status) ? styles.green : styles.red]}>{baker.freespace_min}</Text>
                                                                 <Text style={[styles.td, {width: 120, paddingLeft: 40}]}>{baker.total_points}</Text>
                                                             </View>
                                                         ))}
                                                 </View>
                                             </ScrollView>
                                         </View>
                                     </View>
                                 </ScrollView>
                             }
                         </View>
                         <View style={[styles.header, {justifyContent: 'space-between'}]}>
                             <TouchableOpacity onPress={() => this.navigation.navigate('Tezos')}>
                                 <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                     <Text style={styles.h2}>
                                         Tezos Network Status
                                     </Text>
                                     <SvgXml xml={svgArrow} width="8" height="13" />
                                 </View>
                             </TouchableOpacity>
                             <TouchableOpacity onPress={() => this.tezosUpdate()}>
                                 <SvgXml xml={svgUpdate} width="14" height="14" style={{padding: 7, marginRight: 10}}/>
                             </TouchableOpacity>
                         </View>
                         {!this.state.tezos_update ?
                             <Spinner color='#319DFE' />
                             :
                             <View>
                                 <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                     <View style={{flex: 1, flexDirection: 'row', flexWrap: 'nowrap', backgroundColor: '#121520', borderRadius: 20, padding: 8}}>
                                        <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'column'}}>
                                            <View style={{flex:1 , flexDirection: 'row'}}>
                                                <View style={[styles.tint_tezos, {flexGrow: 1, flexDirection: 'row', alignItems: 'center'}]}>
                                                    <View style={{marginTop: -5, marginLeft: -5, marginRight: 10}}>
                                                        <SvgXml xml={svgCycle} width="45" height="40" />
                                                        <Text style={{position: 'absolute', fontFamily: 'DINNextLTPro_bold', fontSize: normalize(12), left: 0, top: 16, right: 0, textAlign: 'center', color: '#30DD36'}}>{tezos.cycle_percent}%</Text>
                                                    </View>
                                                    <View>
                                                        <Text style={styles.title}>CURRENT CYCLE</Text>
                                                        <Text style={styles.text}>{this.state.tezos.cycle} / -{this.state.tezos.time_value}</Text>
                                                    </View>
                                                </View>
                                                <View style={[styles.tint_tezos, {flex: 1, flexDirection: 'column'}]}>
                                                    <Text style={styles.title}>CIRCULATING SUPPLY</Text>
                                                    <Text style={styles.text}>{this.state.tezos.circulating_supply}</Text>
                                                </View>
                                                <View style={[styles.tint_tezos, {flex: 1, flexDirection: 'column'}]}>
                                                    <Text style={styles.title}>ACTIVE DELEGATORS</Text>
                                                    <Text style={styles.text}>{this.number_format(this.state.tezos.active_delegators, '.', ',')}</Text>
                                                </View>
                                                <View style={[styles.tint_tezos, {flex: 1, flexDirection: 'column'}]}>
                                                    <Text style={styles.title}>ACTIVE BAKERS</Text>
                                                    <Text style={styles.text}>{this.number_format(this.state.tezos.active_bakers, '.', ',')}</Text>
                                                </View>
                                            </View>
                                            <View style={{flex:1, flexDirection: 'row'}}>
                                                <View style={[styles.tint_tezos, {flex: 1, flexDirection: 'column'}]}>
                                                    <Text style={styles.title}>PRICE XTZ/BTC</Text>
                                                    <View>
                                                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                                            <Text style={styles.text}>{this.state.tezos.tezos_price_btc}</Text>
                                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 5}}>
                                                                <Text style={{fontFamily: 'Circe_bold', fontSize: normalize(12), color: changeBtcColor, marginRight: 4}}>{tezos.percent_change_btc_24h}</Text>
                                                                <SvgXml xml={svgBtcChange} />
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={[styles.tint_tezos, {flex: 1, flexDirection: 'column'}]}>
                                                    <Text style={styles.title}>PRICE XTZ/USD</Text>
                                                    <View>
                                                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                                            <Text style={styles.text}>$ {this.state.tezos.tezos_price_usd}</Text>
                                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 5}}>
                                                                <Text style={{fontFamily: 'Circe_bold', fontSize: normalize(12), color: changeColor, marginRight: 4}}>{tezos.percent_change_24h}</Text>
                                                                <SvgXml xml={svgChange} />
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={[styles.tint_tezos, {flex: 1, flexDirection: 'column'}]}>
                                                    <Text style={styles.title}>PUBLIC BAKERS</Text>
                                                    <Text style={styles.text}>{this.number_format(this.state.tezos.public_bakers, '.', ',')}</Text>
                                                </View>
                                                <View style={[styles.tint_tezos, {flex: 1, flexDirection: 'column'}]}>
                                                    <Text style={styles.title}>MARKET CAP</Text>
                                                    <Text style={styles.text}>$ {this.number_format(this.state.tezos.market_cap.toFixed(0), '.', ',')}</Text>
                                                </View>
                                                <View style={[styles.tint_tezos, {flex: 1, flexDirection: 'column'}]}>
                                                    <Text style={styles.title}>STAKING RATIO</Text>
                                                    <Text style={styles.text}>{this.state.tezos.staking_ratio}%</Text>
                                                </View>
                                            </View>
                                        </View>
                                     </View>
                                 </ScrollView>
                             </View>
                         }
                         {!this.state.authorized ?
                             <View>
                                 <TouchableOpacity onPress={() => this.navigation.navigate('Account')}>
                                     <View style={styles.header}>
                                         <Text style={styles.h2}>
                                             Account
                                         </Text>
                                         <SvgXml xml={svgArrow} width="8" height="13" />
                                     </View>
                                 </TouchableOpacity>
                                 <View style={styles.login_form}>
                                     <View>
                                        <SvgXml xml={svgAccount} style={styles.form_icon} width="25" height="25"/>
                                        <TextInput style={styles.login_form_input} placeholder="Your Login" value={this.state.login} onChangeText={(text) => this.setState({login: text})} placeholderTextColor={'#737B9C'}></TextInput>
                                     </View>
                                     <View>
                                         <SvgXml xml={svgPassword} style={styles.form_icon} width="25" height="25"/>
                                        <TextInput style={styles.login_form_input} placeholder="Your Password" value={this.state.password} onChangeText={(text) => this.setState({password: text})} placeholderTextColor={'#737B9C'} secureTextEntry={true}></TextInput>
                                     </View>
                                     <View style={styles.login_buttons}>
                                         <Button style={styles.login_button} onPress={() => this.login()}><Text style={styles.login_text}>Login</Text></Button>
                                         <Button style={styles.register_button} onPress={() => this.openUrl('https://tezos-nodes.com/register')}><Text style={styles.register_text}>Sign in</Text></Button>
                                     </View>
                                 </View>
                             </View>
                             :
                             <View></View>
                         }
                         <TouchableOpacity onPress={() => this.navigation.navigate('Calc', {assetValue: assetValue})}>
                             <View style={styles.header}>
                                 <Text style={styles.h2}>
                                     Calculator rewards
                                 </Text>
                                 <SvgXml xml={svgArrow} width="8" height="13" />
                             </View>
                         </TouchableOpacity>
                         <View style={styles.calculator_block}>
                             <View style={{backgroundColor: '#1E2233', borderRadius: 12, padding: 20, paddingLeft: 15, paddingRight: 15, marginBottom: 15}}>
                                 <Text style={[styles.din_text, {marginLeft: 7.5, marginBottom: -7.5}]}>AMOUNT DELEGATION</Text>
                                 <View style={{margin:7.5}}>
                                     <TextInput style={styles.input} onChangeText={(text)=> this.onChanged(text)} value={`${this.state.assetValue}`} keyboardType='numeric' maxLength={5} keyboardAppearance={'dark'}></TextInput>
                                     <View style={styles.input_icon}>
                                         <Text style={styles.input_icon_text}>XTZ</Text>
                                     </View>
                                 </View>
                             </View>
                             <View style={styles.calculator_block_row}>
                                 <View style={[styles.calculator_block_tint, {marginRight: 10}]}>
                                     <Text style={styles.h4}>TEZOS PRICE</Text>
                                     <Text style={styles.h4_strong}>$ {this.state.tezos.tezos_price_usd}</Text>
                                 </View>
                                 <View style={[styles.calculator_block_tint, {marginLeft: 10}]}>
                                     <Text style={styles.h4}>ASSET VALUE</Text>
                                     <Text style={styles.h4_strong}>$ {(this.state.assetValue*this.state.tezos.tezos_price_usd).toFixed(2)}</Text>
                                 </View>
                             </View>
                             <Button onPress={() => this.navigation.navigate('Calc', {assetValue: assetValue})} style={[styles.login_button, {marginRight: 0, width: '100%'}]}><Text style={styles.login_text}>Calculate Rewards</Text></Button>
                         </View>
                         <TouchableOpacity onPress={() => this.navigation.navigate('Baking')}>
                             <View style={styles.header}>
                                 <Text style={styles.h2}>
                                     Baking Control
                                 </Text>
                                 <SvgXml xml={svgArrow} width="8" height="13" />
                             </View>
                         </TouchableOpacity>
                         <View>
                             <View style={styles.block_info}>
                                 <Text style={styles.text_block_info}>Enter a Tezos Baker's address to view rights baking blocks and endorsers</Text>
                             </View>
                             <View style={styles.block_info}>
                                 <TextInput style={ styles.input_block_info } placeholder="Baking address Tz1BaKi4gAddRes8…" maxLength={36} value={this.state.address} onChangeText={(text) => this.setState({address: text})} placeholderTextColor={ '#737B9C' }/>
                                 <Button style={[styles.login_button, {marginRight: 0, width: '100%'}]} onPress={() => {
                                     this.navigation.navigate('Baking', {address: address})
                                 }}><Text style={styles.login_text}>Verify</Text></Button>
                             </View>
                         </View>
                     </Content>
                 </ScrollView>
             </Container>
         )
     } else {
         return (
            <Container>
                <Head navigation={this.navigation} route={this.route} />
                <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                    <Spinner color='#319DFE' />
                </Content>
            </Container>
         )
     }
  }
}
const {width, height} = Dimensions.get('window');
if(width > 350) {
    var gee = {
        table_text: {
            color: '#EAECF2',
            fontFamily: 'Circe',
            fontSize: normalize(15),
            flex: 1,
            flexWrap: 'wrap'
        },
        td: {
            paddingLeft: 15,
            paddingRight: 0,
            color: '#EAECF2',
            fontFamily: 'Circe',
            fontSize: normalize(15),
            flex: 1,
            flexDirection: 'row'
        },
        tint_text: {
            fontFamily: 'Circe',
            fontSize: normalize(14),
        },
    }
} else {
    var gee = {
        table_text: {
            color: '#EAECF2',
            fontFamily: 'Circe',
            fontSize: normalize(15),
            flex: 1,
            flexWrap: 'wrap'
        },
        td: {
            paddingLeft: 15,
            paddingRight: 0,
            color: '#EAECF2',
            fontFamily: 'Circe',
            fontSize: normalize(15),
            flex: 1,
            flexDirection: 'row'
        },
        tint_text: {
            fontFamily: 'Circe',
            fontSize: normalize(12),
        },
    }
}

const styles = StyleSheet.create(Object.assign(gee, {
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: normalize(26),
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_news: {
        backgroundColor: '#121520',
        borderRadius: 20,
        padding: 15,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    block_news_text: {
        flex: 1,
        flexWrap: 'wrap',
        color: '#EAECF2',
        fontFamily: 'Circe',
        fontSize: normalize(18),
        lineHeight: 24,
        marginLeft: 16
    },
    tbodytr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 61,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#30333F',
        backgroundColor: '#121520'
    },
    theadtr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 61,
        justifyContent: 'center',
        flexWrap: 'nowrap'
    },
    th: {
        backgroundColor: '#0D0F17',
        paddingTop: 23,
        paddingBottom: 23,
        paddingLeft: 15,
        fontSize: normalize(15),
        color: '#737B9C',
        fontFamily: 'DINNextLTPro',
        ...Platform.select({
          ios: {
            paddingTop: 24,
          },
        }),
    },
    tint: {
        fontFamily: 'Circe',
        fontSize: normalize(16),
        paddingTop: 4,
        paddingBottom: 2,
        borderWidth: 1,
        borderRadius: 5,
        flex: 1,
    },
    green: {
        color: '#30DD36',
        borderColor: '#30DD36'
    },
    red: {
        color: '#F91525',
        borderColor: '#F91525'
    },
    yellow: {
        color: '#FFAD15',
        borderColor: '#FFAD15'
    },
    login_form: {
        backgroundColor: '#121520',
        borderRadius: 20,
        padding: 16,
    },
    login_form_input: {
        backgroundColor: '#090A11',
        borderRadius: 12,
        marginBottom: 10,
        paddingLeft: 50,
        height: 60,
        color: '#737B9C',
        fontFamily: 'Circe',
        fontSize: normalize(16),
    },
    form_icon: {
        position: 'absolute',
        top: 17,
        left: 14,
        zIndex: 1
    },
    login_buttons: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    login_button: {
        flexGrow: 1,
        backgroundColor: '#319DFE',
        marginRight: 10,
        height: 60,
        borderRadius: 12,
        flex: 1,
        justifyContent: 'center',
        marginBottom: 7
    },
    login_text: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(16),
        color: 'white'
    },
    register_button: {
        flexGrow: 1,
        backgroundColor: 'transparent',
        marginRight: 10,
        height: 60,
        borderRadius: 12,
        flex: 1,
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#333952',
        marginBottom: 7
    },
    register_text: {
        color: '#6F7693',
        fontFamily: 'Circe_bold',
        fontSize: normalize(16),
    },
    calculator_block: {
        backgroundColor: '#121520',
        borderRadius: 20,
        padding: 16
    },
    calculator_block_row: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 22
    },
    calculator_block_tint: {
        flexGrow: 1,
        backgroundColor: '#1E2233',
        borderRadius: 12,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 77
    },
    h4: {
        fontFamily: 'DINNextLTPro',
        fontSize: normalize(10),
        color: '#737B9C'
    },
    h4_strong: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(22),
        color: '#EAECF2',
        ...Platform.select({
          ios: {
            marginBottom: -5,
          },
        }),
    },
    tint_tezos: {
        flex: 1,
        margin: 8,
        backgroundColor: '#1E2233',
        borderRadius: 12,
        paddingTop: 19,
        paddingHorizontal: 15,
        paddingBottom: 10
    },
    title: {
        fontFamily: "DINNextLTPro",
        fontSize: normalize(10),
        color: '#737B9C',
    },
    text: {
        fontFamily: "Circe_bold",
        fontSize: normalize(22),
        color: '#EAECF2',
    },
    din_text: {
        fontFamily: 'DINNextLTPro',
        fontSize: normalize(10),
        color: '#737B9C',
    },
    input_bg: {
        padding: 15.5
    },
    input: {
        fontFamily: 'Circe',
        height: 55,
        borderRadius: 12,
        backgroundColor: '#090A11',
        marginTop: 3,
        paddingLeft: 20,
        fontSize: normalize(22),
        color: '#EAECF2'
    },
    input_icon: {
        position: 'absolute',
        height: 47,
        backgroundColor: '#1E2233',
        top: 7,
        right: 4,
        borderRadius: 9,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    input_icon_text: {
        color: '#737B9C',
        fontWeight: '700'
    },
    block_info: {
        backgroundColor: "#121520",
        borderRadius: 20,
        padding: 20,
        marginBottom: 11
    },
    text_block_info: {
        color: '#737B9C',
        fontSize: normalize(18),
        fontFamily: 'Circe',
        lineHeight: 22
    },
    input_block_info: {
        backgroundColor: '#090A11',
        borderRadius: 12,
        marginBottom: 15,
        paddingLeft: 24,
        height: 60,
        color: '#737B9C',
        fontFamily: 'Circe',
        fontSize: normalize(16),
    },
}));
