import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Text, Linking} from 'react-native';
import { Content, Spinner } from 'native-base';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import { normalize } from '../../normalize';

export default class Statistic extends Component {
    constructor(props) {
        super(props);
        this.baker = props.baker;
        this.state = {
            baker: {},
            last_endoresment: '',
            last_baking: '',
            next_endoresment: '',
            next_baking: '',
            loading: false,
        }
    }

    componentDidMount() {

        const url = "https://api.tezos-nodes.com/v1/baker/" + this.baker.address;
        fetch(url)
        .then(response => response.json())
        .then(json => this.setState({ baker: json }));
        this.last_endoresment = this.setState({ last_endoresment: this.HowDate(this.baker.last_endoresment, false)});
        this.last_baking = this.setState({ last_baking: this.HowDate(this.baker.last_baking, false)});
        this.next_endoresment = this.setState({ next_endoresment: this.HowDate(this.baker.next_endoresment, true)});
        this.next_baking = this.setState({ next_baking: this.HowDate(this.baker.next_baking, true)});
        this.loading = true;
   }

   componentWillUnMount() {
    clearInterval(this.timer);
  }

    HowDate(dat, diff) {
        if(diff == true) {
            var date = new Date;
            var d = new Date(dat);
        } else {
            var date = new Date(dat);
            var d = new Date;
        }
        var myDate = date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear() + ' ' + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        var a = myDate.split (' '),
        b = a [0].split  ('.'),
        c = a [1].split  (':'),

        T     = [],                  C     = [];
        T [0] =           b [2],     C [0] = d.getFullYear (),
        T [1] =           b [1]  , C [1] = d.getMonth    (),
        T [2] = parseInt (b [0])   , C [2] = d.getDate     (),
        T [3] = parseInt (c [0])   , C [3] = d.getHours    (),
        T [4] = parseInt (c [1])   , C [4] = d.getMinutes  (),
        T [5] = parseInt (c [2])   , C [5] = d.getSeconds  ();

        for (var D = [], j = 0; j < 6; j++) D [j] = C [j] - T [j];

        if (D [5] < 0) D [5] += 60,                                                   D [4]--;
        if (D [4] < 0) D [4] += 60,                                                   D [3]--;
        if (D [3] < 0) D [3] += 24,                                                   D [2]--;
        if (D [2] < 0) D [2] = C [2] + new Date (C [0], C [1], 0).getDate () - T [2], D [1]--;
        if (D [1] < 0) D [1] = C [1] + 12 - T [1],                                    D [0]--;
        var w = ['Y', 'M', 'D', 'h', 'm', 's'];
        var E = '';
        for(j = 0; j < 6; j++) {
            if(D[j] !== 0) {
                E += D[j] + w[j] + " ";
            }
        }
        if(D[0] < 0 || E == '') {
            return '0s';
        }
        return E;
    }

    render() {
        const { baker, last_endoresment, next_endoresment, last_baking, next_baking } = this.state;
        if(this.loading) {
            return (
                <View>
                    <View style={styles.header}>
                        <Text style={styles.h2}>
                            Baker’s personal statistics
                        </Text>
                    </View>
                    <View style={[styles.info_block, {maxHeight: 250}]}>
                        <GestureHandlerScrollView>
                            <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>LIFE TIME</Text>
                                    <Text style={styles.text}>{baker.lifetime} cycles</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>YIELD</Text>
                                    <Text style={styles.text}>{parseFloat(baker.yield).toFixed(2)}%</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>EFFICIENCY LAST YEAR</Text>
                                    <Text style={[styles.text, baker.efficiency > 96 ? styles.green : baker.efficiency > 90 ? styles.yellow : styles.red]}>{baker.efficiency}%</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>LAST 10 CYCLES</Text>
                                    <Text style={[styles.text, baker.efficiency_last10cycle > 96 ? styles.green : baker.efficiency_last10cycle > 90 ? styles.yellow : styles.red]}>{baker.efficiency_last10cycle}%</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>STAKING BALANCE</Text>
                                    <Text style={styles.text}>{(baker.staking_balance/1000).toFixed(2)}k</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>EVALUATED BALANCE</Text>
                                    <Text style={styles.text}>{(baker.evaluated_balance/1000).toFixed(2)}k</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>TOTAL POINTS</Text>
                                    <Text style={styles.text}>{baker.total_points}/100</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>DELEGATORS</Text>
                                    <Text style={styles.text}>{baker.rolls}</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>NEXT BAKING</Text>
                                    <Text style={styles.text}>{next_baking}</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>NEXT ENDORSEMENT</Text>
                                    <Text style={styles.text}>{next_endoresment}</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>LAST BAKING</Text>
                                    <Text style={styles.text}>{last_baking}</Text>
                                </View>
                                <View style={styles.tint}>
                                    <Text style={styles.title}>LAST ENDORSEMENT</Text>
                                    <Text style={styles.text}>{last_endoresment}</Text>
                                </View>
                            </View>
                        </GestureHandlerScrollView>
                    </View>
                </View>
            );
        } else {
            return (
                <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                    <Spinner color='#319DFE' />
                </Content>
            )
        }
    }
}
const {width, height} = Dimensions.get('window');
if(width > 350) {
    var gee = {
        h2: {
            color: '#EAECF2',
            fontSize: normalize(26),
            fontFamily: 'Circe_bold',
            marginRight: 11
        },
        title: {
            fontFamily: "DINNextLTPro",
            fontSize: normalize(10),
            color: '#737B9C',
        },
        text: {
            fontFamily: "Circe_bold",
            fontSize: normalize(22),
            color: '#EAECF2',
        },
    }
} else {
    var gee = {
        h2: {
            color: '#EAECF2',
            fontSize: normalize(20),
            fontFamily: 'Circe_bold',
            marginRight: 11
        },
        title: {
            fontFamily: "DINNextLTPro",
            fontSize: normalize(9),
            color: '#737B9C',
        },
        text: {
            fontFamily: "Circe_bold",
            fontSize: normalize(17),
            color: '#EAECF2',
        },
    }
}
const styles = StyleSheet.create(Object.assign(gee, {
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },

    info_block: {
        padding: 7.5,
        backgroundColor: '#121520',
        borderRadius: 20,
        marginBottom: 29,
        paddingBottom: 5,
    },
    tint: {
        flex: 1,
        flexGrow: 1,
        minWidth: '40%',
        margin: 7.5,
        backgroundColor: '#1E2233',
        borderRadius: 12,
        padding: 19,
        paddingBottom: 10
    },
    green: {
        color: '#30DD36',
    },
    red: {
        color: '#F91525'
    },
    yellow: {
        color: '#FFAD15'
    }
})
)