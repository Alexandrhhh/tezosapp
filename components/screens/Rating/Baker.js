import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Platform, Clipboard, WebView, Linking} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {
    Container,
    Header,
    Content,
    Footer,
    FooterTab,
    Button,
    Text,
    Spinner,
    Tab,
    Tabs,
    ScrollableTab,
    Toast
} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import AppLink from 'react-native-app-link';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import * as WebBrowser from 'expo-web-browser';

import Head from '../../Header';
import { normalize } from '../../normalize';
import {SvgXml} from 'react-native-svg';
import Contacts from './Contacts';
import Statistic from './Statistic';
import Vote from './Vote';
import Projects from './Projects';
import {Link} from "@react-navigation/native";

export default class Bakerscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          baker: {
              freespace_percent: 0,
              payouts: {
                  title: ''
              },
              contacts: {}
          },
          loading: false,
          copy_btn: false,
          notifier_btn: false,
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }
    ucFirst(str) {
      if (!str) return str;

      return str[0].toUpperCase() + str.slice(1);
    }

    async openUrl(uri) {
        await WebBrowser.openBrowserAsync(uri);
    }

    number_format(number, decimals, dec_point, thousands_sep) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          var k = Math.pow(10, prec);
          return '' + (Math.round(n * k) / k)
            .toFixed(prec);
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '')
        .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
          .join('0');
      }
      return s.join(dec);
    }

    componentDidMount() {
        const url = "https://api.tezos-nodes.com/v1/baker/" + this.route.params.tz;
        fetch(url)
        .then(response => response.json())
        .then(json => this.setState({ baker: json }))
        this.loading = true;
   }

    render() {

        const { baker, copy_btn, notifier_btn } = this.state;
        if(baker.freespace_percent > 100) {
            var freespace_percent = 100;
        } else {
            var freespace_percent = baker.freespace_percent;
        }
        var color = '';
        if(baker.freespace_percent <= 80) {
            color = '#30DD36';
        } else if(baker.freespace_percent <= 91) {
            color = "#FFAD15";
        } else {
            color = "#F91525";
        }
        const svgFreespace = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="51" height="48" viewBox="0 0 51 48" fill="none"><path d="M39.6318 44.8945C44.8713 40.89 48.3111 34.629 48.4737 27.5232C48.6132 21.4253 46.3133 15.8386 42.4659 11.6969C38.4751 7.40072 32.8194 4.65924 26.4941 4.51455C14.071 4.23037 3.76963 14.071 3.48545 26.4941C3.31451 33.9667 6.8068 40.6716 12.3209 44.8906" stroke="' + color + '" stroke-dashoffset="0" stroke-width="6" stroke-linejoin="bevel" stroke-dasharray="110" /><path d="M39.6318 44.8945C44.8713 40.89 48.3111 34.629 48.4737 27.5232C48.6132 21.4253 46.3133 15.8386 42.4659 11.6969C38.4751 7.40072 32.8194 4.65924 26.4941 4.51455C14.071 4.23037 3.76963 14.071 3.48545 26.4941C3.31451 33.9667 6.8068 40.6716 12.3209 44.8906" stroke="#C4DFFF" stroke-dashoffset="' + freespace_percent*1.07 + '" stroke-width="6" stroke-linejoin="bevel" stroke-dasharray="110" /><path d="M39.6318 44.8945C44.8713 40.89 48.3111 34.629 48.4737 27.5232C48.6132 21.4253 46.3133 15.8386 42.4659 11.6969C38.4751 7.40072 32.8194 4.65924 26.4941 4.51455C14.071 4.23037 3.76963 14.071 3.48545 26.4941C3.31451 33.9667 6.8068 40.6716 12.3209 44.8906" stroke="#1E2233" stroke-width="7" stroke-linejoin="bevel" stroke-dasharray="2.5 1.5" /></svg>';
        const svgCopy = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="17" height="21" viewBox="0 0 17 21" fill="none"><path d="M10.6891 21H3.15545C1.41549 21 0 19.5281 0 17.7188V6.60352C0 4.79418 1.41549 3.32227 3.15545 3.32227H10.6891C12.4291 3.32227 13.8445 4.79418 13.8445 6.60352V17.7188C13.8445 19.5281 12.4291 21 10.6891 21ZM3.15545 4.96289C2.28555 4.96289 1.57773 5.69893 1.57773 6.60352V17.7188C1.57773 18.6233 2.28555 19.3594 3.15545 19.3594H10.6891C11.559 19.3594 12.2668 18.6233 12.2668 17.7188V6.60352C12.2668 5.69893 11.559 4.96289 10.6891 4.96289H3.15545ZM17 15.668V3.28125C17 1.47192 15.5845 0 13.8445 0H5.08817C4.65244 0 4.2993 0.367218 4.2993 0.820312C4.2993 1.27341 4.65244 1.64062 5.08817 1.64062H13.8445C14.7145 1.64062 15.4223 2.37666 15.4223 3.28125V15.668C15.4223 16.1211 15.7754 16.4883 16.2111 16.4883C16.6469 16.4883 17 16.1211 17 15.668Z" fill="#319DFE"/></svg>';
        const svgCopyActive = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="17" height="21" viewBox="0 0 17 21" fill="none"><path d="M10.6891 21H3.15545C1.41549 21 0 19.5281 0 17.7188V6.60352C0 4.79418 1.41549 3.32227 3.15545 3.32227H10.6891C12.4291 3.32227 13.8445 4.79418 13.8445 6.60352V17.7188C13.8445 19.5281 12.4291 21 10.6891 21ZM3.15545 4.96289C2.28555 4.96289 1.57773 5.69893 1.57773 6.60352V17.7188C1.57773 18.6233 2.28555 19.3594 3.15545 19.3594H10.6891C11.559 19.3594 12.2668 18.6233 12.2668 17.7188V6.60352C12.2668 5.69893 11.559 4.96289 10.6891 4.96289H3.15545ZM17 15.668V3.28125C17 1.47192 15.5845 0 13.8445 0H5.08817C4.65244 0 4.2993 0.367218 4.2993 0.820312C4.2993 1.27341 4.65244 1.64062 5.08817 1.64062H13.8445C14.7145 1.64062 15.4223 2.37666 15.4223 3.28125V15.668C15.4223 16.1211 15.7754 16.4883 16.2111 16.4883C16.6469 16.4883 17 16.1211 17 15.668Z" fill="white"/></svg>';
        const svgNotifier = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="18" height="23" viewBox="0 0 18 23" fill="none"><path d="M15.8318 14.9442V9.5247C15.8318 6.52097 13.8282 3.98105 11.1001 3.19712V2.12143C11.1001 0.951666 10.158 0 8.99996 0C7.84195 0 6.89985 0.951666 6.89985 2.12143V3.19717C4.17179 3.98105 2.1681 6.52097 2.1681 9.5247V14.9442C0.950238 15.0812 0 16.127 0 17.3931V17.5878C0 18.9468 1.0945 20.0525 2.43981 20.0525H6.31267V20.2854C6.31267 21.7822 7.51821 23 9 23C10.4818 23 11.6873 21.7822 11.6873 20.2854V20.0525H15.5601C16.9055 20.0525 18 18.9468 18 17.5878V17.3931C17.9999 16.127 17.0496 15.0812 15.8318 14.9442ZM8.2815 2.12143C8.2815 1.72122 8.60382 1.39563 9 1.39563C9.39618 1.39563 9.7185 1.72122 9.7185 2.12143V2.96016C9.58687 2.95212 9.45431 2.94749 9.32072 2.94749H8.67924C8.54565 2.94749 8.41308 2.95207 8.2815 2.96012V2.12143ZM3.5497 9.5247C3.5497 6.66755 5.85076 4.34312 8.67919 4.34312H9.32067C12.1491 4.34312 14.4502 6.66755 14.4502 9.5247V14.9286H3.5497V9.5247ZM8.99996 21.6043C8.27998 21.6043 7.69422 21.0126 7.69422 20.2853V20.0524H10.3056V20.2853H10.3057C10.3057 21.0127 9.71993 21.6043 8.99996 21.6043ZM16.6184 17.5878H16.6183C16.6183 18.1772 16.1435 18.6568 15.5601 18.6568H2.43981C1.85632 18.6568 1.3816 18.1772 1.3816 17.5878V17.3931C1.3816 16.8036 1.85632 16.3241 2.43981 16.3241H2.85894H15.141H15.5601C16.1436 16.3241 16.6184 16.8036 16.6184 17.3931V17.5878Z" fill="#319DFE"/></svg>';
        const svgNotifierActive = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="18" height="23" viewBox="0 0 18 23" fill="none"><path d="M15.8318 14.9442V9.5247C15.8318 6.52097 13.8282 3.98105 11.1001 3.19712V2.12143C11.1001 0.951666 10.158 0 8.99996 0C7.84195 0 6.89985 0.951666 6.89985 2.12143V3.19717C4.17179 3.98105 2.1681 6.52097 2.1681 9.5247V14.9442C0.950238 15.0812 0 16.127 0 17.3931V17.5878C0 18.9468 1.0945 20.0525 2.43981 20.0525H6.31267V20.2854C6.31267 21.7822 7.51821 23 9 23C10.4818 23 11.6873 21.7822 11.6873 20.2854V20.0525H15.5601C16.9055 20.0525 18 18.9468 18 17.5878V17.3931C17.9999 16.127 17.0496 15.0812 15.8318 14.9442ZM8.2815 2.12143C8.2815 1.72122 8.60382 1.39563 9 1.39563C9.39618 1.39563 9.7185 1.72122 9.7185 2.12143V2.96016C9.58687 2.95212 9.45431 2.94749 9.32072 2.94749H8.67924C8.54565 2.94749 8.41308 2.95207 8.2815 2.96012V2.12143ZM3.5497 9.5247C3.5497 6.66755 5.85076 4.34312 8.67919 4.34312H9.32067C12.1491 4.34312 14.4502 6.66755 14.4502 9.5247V14.9286H3.5497V9.5247ZM8.99996 21.6043C8.27998 21.6043 7.69422 21.0126 7.69422 20.2853V20.0524H10.3056V20.2853H10.3057C10.3057 21.0127 9.71993 21.6043 8.99996 21.6043ZM16.6184 17.5878H16.6183C16.6183 18.1772 16.1435 18.6568 15.5601 18.6568H2.43981C1.85632 18.6568 1.3816 18.1772 1.3816 17.5878V17.3931C1.3816 16.8036 1.85632 16.3241 2.43981 16.3241H2.85894H15.141H15.5601C16.1436 16.3241 16.6184 16.8036 16.6184 17.3931V17.5878Z" fill="white"/></svg>';
        const svgLedger = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="32" height="32" viewBox="0 0 32 32" fill="none"><path fill-rule="evenodd" clip-rule="evenodd" d="M5 0C2.23858 0 0 2.23858 0 5V8H8V0H5ZM12 0V20H32V5C32 2.23858 29.7614 0 27 0H12ZM32 24H24V32H27C29.7614 32 32 29.7614 32 27V24ZM20 32V24H12V32H20ZM8 32V24H0V27C0 29.7614 2.23858 32 5 32H8ZM0 20H8V12H0V20Z" fill="white"/></svg>';

        if(this.loading) {
            return (
                <Container>
                <Head sub={true} navigation={this.navigation} route={this.route}/>
                        <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                            <View style={{padding: 10, paddingTop: 0}}>
                                <View style={[styles.header, {justifyContent: 'space-between'}]}>
                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                        <View style={{width: 34, height: 34}}>
                                            <Image source={{uri: baker.logo}} style={{flex:1, width: null, height: null, resizeMode: 'contain', marginRight: 7}} />
                                        </View>
                                        <Text style={styles.h2}>{baker.name}</Text>
                                    </View>
                                    {baker.pro_status ? <Image source={require('../../../assets/icons/pro.png')} width="41" heihgt="26" /> : <Text></Text>}
                                </View>
                                <View style={styles.info_block}>
                                    <View style={[styles.accept_delegator, baker.deletation_status ? styles.accept_delegator_green : styles.accept_delegator_red]}>
                                        {
                                            baker.deletation_status ?
                                            <Text style={styles.text_accept}>Accepting new delegations</Text>
                                             :
                                            <Text style={styles.text_noaccept}>Not accepting new delegations</Text>
                                        }
                                    </View>
                                    <Text style={styles.delegation_address}>
                                        Delegation Address:
                                    </Text>
                                    <ScrollView nestedScrollEnabled={true} horizontal={true}>
                                        <TouchableOpacity onPress={() => this.openUrl('https://tzstats.com/' + baker.address)}>
                                            <Text style={styles.address}>
                                                {baker.address}
                                            </Text>
                                        </TouchableOpacity>
                                    </ScrollView>
                                    <View style={[styles.row, styles.justify_between]}>
                                        <Text style={styles.delegation_address}>Copy</Text>
                                        <Text style={styles.delegation_address}>Delegation</Text>
                                        <Text style={styles.delegation_address}>Notifier</Text>
                                    </View>
                                    <View style={styles.buttons_copy}>
                                        <TouchableOpacity style={[styles.button, styles.button_copy, copy_btn ? styles.button_copy_active : '']} onPress={() => {Clipboard.setString(baker.address); this.setState({ copy_btn: true})}}>
                                            <SvgXml xml={svgCopyActive} width="17" height="21" style={copy_btn ? '' : {display:'none'} } />
                                            <SvgXml xml={svgCopy} width="17" height="21" style={copy_btn ? {display:'none'} : '' } />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.button} onPress={() => {Linking.openURL('https://link.trustwallet.com/stake_delegate?coin=1729&id=1729')}}>
                                            <Image source={require('../../../assets/icons/trust.png')} style={{resizeMode:'cover',width:null, paddingTop: '100%', height:null, flex:1}}/>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.button} onPress={() => AppLink.openInStore({ appName: 'Huobi Wallet - Safe & Reliable', appStoreId: '1433883012', appStoreLocale: 'us', playStoreId: 'com.huobionchainwallet.gp' })}>
                                            <Image source={require('../../../assets/icons/huobi.png')} style={{resizeMode:'cover',width:null,height:null,paddingTop: '100%',  flex:1}}/>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.button} onPress={() => AppLink.openInStore({ appName: 'Magma Wallet', appStoreId: '1512745852', appStoreLocale: 'us', playStoreId: 'io.camlcase.smartwallet' })}>
                                            <Image source={require('../../../assets/icons/Magma.png')} style={{resizeMode:'cover',width:null,height:null,paddingTop: '100%',  flex:1}}/>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[styles.button, styles.button_notifier, notifier_btn ? styles.button_copy_active : '']} onPress={() => {this.openUrl(baker.notifier); this.setState({ notifier_btn: true})}}>
                                            <SvgXml xml={svgNotifierActive} width="18" height="23" style={notifier_btn ? '' : {display:'none'} } />
                                            <SvgXml xml={svgNotifier} width="18" height="23" style={notifier_btn ? {display:'none'} : '' } />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.tints}>
                                        <View style={[styles.tint, {minWidth: '50%'}]}>
                                            <Text style={styles.title}>REWARDS PAYOUTS</Text>
                                            <Text style={styles.text}>{this.ucFirst(baker.payouts.title.replace('First rewards ', '').replace(' payout interval', ''))}</Text>
                                        </View>
                                        <View style={[styles.tint, {minWidth: '35%'}]}>
                                            <Text style={styles.title}>FEE</Text>
                                            <Text style={styles.text}>{(baker.fee*100).toFixed(2)}%</Text>
                                        </View>
                                        <View style={[styles.tint, {minWidth: '50%', flexDirection: 'row', alignItems: 'center'}]}>
                                            <View>
                                                <SvgXml width="50" height="48" xml={svgFreespace} style={{marginTop:-5}} />
                                                <Text style={{position: 'absolute', fontFamily: 'DINNextLTPro_bold', fontSize: normalize(12), left: 0, top: 15, right: 0, textAlign: 'center', color: color}}>{freespace_percent.toFixed(0)}%</Text>
                                            </View>
                                            <View style={{marginLeft: 20}}>
                                                <Text style={styles.title}>FREE SPACE</Text>
                                                <Text style={styles.text}>{this.number_format((baker.freespace/1000).toFixed(0), 0, '.', ',')}k</Text>
                                            </View>
                                        </View>
                                        <View style={[styles.tint, {minWidth: '35%'}]}>
                                            <Text style={styles.title}>MIN DELEGATION</Text>
                                            <Text style={styles.text}>{baker.min_delegations_amount} XTZ</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        <Tabs style={{backgroundColor: "#121520", width: '100%'}} tabContainerStyle={{elevation: 0, borderBottomWidth: 0}} tabBarUnderlineStyle={{borderWidth: 2, borderColor: '#319DFE', borderRadius: 4}}>
                          <Tab heading="Contacts" style={{backgroundColor: '#1E2233', padding: 10, paddingTop: 0}} activeTabStyle={{backgroundColor: "#121520"}} tabStyle={{backgroundColor: "#121520"}} textStyle={{fontFamily: 'Circe', color: '#fff', lineHeight: 53, fontSize: normalize(18)}} activeTextStyle={{fontFamily: 'Circe', color: '#fff', lineHeight: 53, fontSize: normalize(18)}}>
                            <Contacts baker={baker} />
                          </Tab>
                          <Tab heading="Statistic" style={{backgroundColor: '#1E2233', padding: 10, paddingTop: 0}} activeTabStyle={{backgroundColor: "#121520"}} tabStyle={{backgroundColor: "#121520"}} textStyle={{fontFamily: 'Circe', color: '#fff', lineHeight: 53, fontSize: normalize(18)}} activeTextStyle={{fontFamily: 'Circe', color: '#fff', lineHeight: 53, fontSize: normalize(18)}}>
                            <Statistic baker={baker} />
                          </Tab>
                          <Tab heading="Vote" style={{backgroundColor: '#1E2233', padding: 10, paddingTop: 0}} activeTabStyle={{backgroundColor: "#121520"}} tabStyle={{backgroundColor: "#121520"}} textStyle={{fontFamily: 'Circe', color: '#fff', lineHeight: 53, fontSize: normalize(18)}} activeTextStyle={{fontFamily: 'Circe', color: '#fff', lineHeight: 53, fontSize: normalize(18)}}>
                            <Vote baker={baker} />
                          </Tab>
                          <Tab heading="Projects" style={{backgroundColor: '#1E2233', padding: 10, paddingTop: 0}} activeTabStyle={{backgroundColor: "#121520"}} tabStyle={{backgroundColor: "#121520"}} textStyle={{fontFamily: 'Circe', color: '#fff', lineHeight: 53, fontSize: normalize(18)}} activeTextStyle={{fontFamily: 'Circe', color: '#fff', lineHeight: 53, fontSize: normalize(18)}}>
                            <Projects baker={baker} />
                          </Tab>
                        </Tabs>
                        </Content>
                </Container>
            )
        } else {
            return (
                       <Container>
                              <Head sub={true} navigation={this.navigation} route={this.route}/>
                           <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                               <Spinner color='#319DFE' />
                           </Content>
                       </Container>
            )
        }
    }
}

const {width, height} = Dimensions.get('window');
if(width > 350) {
    var gee = {
        h2: {
            color: '#EAECF2',
            fontSize: normalize(26),
            fontFamily: 'Circe_bold',
            marginRight: 11
        },
        delegation_address: {
            fontFamily: 'DINNextLTPro',
            fontSize: normalize(18),
            color: '#737B9C',
            flexWrap: 'nowrap',
            marginBottom: 5,
        },
        address: {
            fontFamily: 'Circe',
            fontSize: normalize(16),
            color: '#EAECF2',
            marginBottom: 15,
        },
        button_copy: {
            borderWidth: 1.5,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 12,
            borderColor: '#319DFE'
        },
        button_ledger: {
            flexGrow: 1,
            flex: 1,
            flexDirection: 'row',
            height: 60,
            alignItems: 'center',
            backgroundColor: '#319DFE',
            borderRadius: 12,
            paddingLeft: 14
        },
        button_notifier: {
            borderWidth: 1.5,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 12,
            borderColor: '#319DFE'
        },
        title: {
            fontFamily: "DINNextLTPro",
            fontSize: normalize(10),
            color: '#737B9C',
        },
        text: {
            fontFamily: "Circe_bold",
            fontSize: normalize(22),
            color: '#EAECF2',
        },
    }
} else {
    var gee = {
        h2: {
            color: '#EAECF2',
            fontSize: normalize(20),
            fontFamily: 'Circe_bold',
            marginRight: 11
        },
        delegation_address: {
            fontFamily: 'DINNextLTPro',
            fontSize: normalize(15),
            color: '#737B9C',
            flexWrap: 'nowrap',
            marginBottom: 5,
        },
        address: {
            fontFamily: 'Circe',
            fontSize: normalize(13),
            color: '#EAECF2',
            marginBottom: 15,
        },
        button_copy: {
            borderWidth: 1.5,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 12,
            borderColor: '#319DFE'
        },
        button_ledger: {
            flexGrow: 1,
            flex: 1,
            flexDirection: 'row',
            height: 50,
            alignItems: 'center',
            backgroundColor: '#319DFE',
            borderRadius: 12,
            paddingLeft: 14
        },
        button_notifier: {
            borderWidth: 1.5,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 12,
            borderColor: '#319DFE'
        },
        title: {
            fontFamily: "DINNextLTPro",
            fontSize: normalize(9),
            color: '#737B9C',
        },
        text: {
            fontFamily: "Circe_bold",
            fontSize: normalize(17),
            color: '#EAECF2',
        },
    }
}
const styles = StyleSheet.create(Object.assign(gee, {
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    info_block: {
        padding: 15,
        backgroundColor: '#121520',
        borderRadius: 20,
        marginBottom: 29
    },
    accept_delegator: {
        height: 44,
        borderRadius: 14,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: -7,
        marginBottom: 22
    },
    accept_delegator_green: {
        backgroundColor: '#44C948',
    },
    accept_delegator_red: {
        backgroundColor: '#C12530'
    },
    text_noaccept: {
        fontFamily: 'Circe',
        fontSize: normalize(16),
        color: '#EAECF2'
    },
    text_accept: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(16),
        color: '#EAECF2'
    },
    buttons_copy: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: -5
    },
    button_copy_active: {
        backgroundColor: '#319DFE'
    },
    tints: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
        marginTop: 10,
        padding: 7.5,
        margin: -15
    },
    tint: {
        flex: 1,
        flexGrow: 1,
        height: 77,
        margin: 7.5,
        backgroundColor: '#1E2233',
        borderRadius: 12,
        padding: 19,
        paddingBottom: 16,
        justifyContent: 'center'
    },
    button: {
        flex: 1,
        flexGrow: 1,
        margin: 5,
        borderRadius: 12,
        overflow: 'hidden',
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    },
    justify_between: {
        justifyContent: 'space-between'
    },
    title_button: {
        fontFamily: 'Circe',
        fontSize: normalize(16),
        color: '#737B9C'
    }
}))
