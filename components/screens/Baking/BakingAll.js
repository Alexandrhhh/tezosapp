import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Linking, AsyncStorage} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../../Header';
import { normalize } from '../../normalize';
import * as WebBrowser from 'expo-web-browser';

export default class BakingAllScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            height: 0,
            current_cycle: 0,
            current_baking: 0,
            current_baking_miss: 0,
            current_baking_all: 0,
            current_endorse: 0,
            current_endorse_all: 0,
            cycles: []
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    async getInfo() {
        await fetch('https://api.tzstats.com/explorer/tip')
            .then(response => response.json())
            .then(json => {
                this.setState({current_cycle: json.cycle, height: json.height})
            })
    }

    async getData() {
        await fetch('https://api.tzstats.com/tables/rights?address=' + this.route.params.address+ '&cycle=' + this.state.current_cycle + '&limit=10000&type=endorsing&columns=is_missed')
            .then((response) => response.json())
            .then((json) => {
                let i = 0;
                json.map((item) => {
                    if(item[0] == 1) {
                        i++;
                    }
                })
                fetch('https://api.tzstats.com/tables/rights?address=' + this.route.params.address+ '&cycle=' + this.state.current_cycle + '&limit=10000&type=endorsing&columns=is_missed&height.lt=' + this.state.height)
                    .then((response) => response.json())
                    .then((j) => {
                        this.setState({current_endorse_all: json.length, current_endorse_miss: i, current_endorse: j.length})
                    })
            })
        await fetch('https://api.tzstats.com/tables/rights?address=' + this.route.params.address+ '&cycle=' + this.state.current_cycle + '&limit=10000&type=baking&priority=0&columns=is_missed')
            .then((response) => response.json())
            .then((json) => {
                let i = 0;
                json.map((item) => {
                    if(item[0] == 1) {
                        i++;
                    }
                })
                fetch('https://api.tzstats.com/tables/rights?address=' + this.route.params.address+ '&cycle=' + this.state.current_cycle + '&limit=10000&type=baking&priority=0&columns=is_missed&height.lt=' + this.state.height)
                    .then((response) => response.json())
                    .then((j) => {
                        this.setState({current_baking_all: json.length, current_baking_miss: i, current_baking: j.length})
                    })
            })
    }

    async getCycle(cycle) {
        var e, b;
        await fetch('https://api.tzstats.com/tables/rights?address=' + this.route.params.address+ '&cycle=' + cycle + '&limit=10000&type=endorsing&columns=is_missed')
            .then((response) => response.json())
            .then((endorse) => {
                e = endorse.length;
            })
        await fetch('https://api.tzstats.com/tables/rights?address=' + this.route.params.address+ '&cycle=' + cycle + '&limit=10000&type=baking&priority=0&columns=is_missed')
            .then((response) => response.json())
            .then((baking) => {
                b = baking.length;
            })
        let old = this.state.cycles;
        old.push({
            cycle: cycle,
            baking: b,
            endorse: e
        })
    }

    async componentDidMount() {
        await this.getInfo();
        await this.getData();
        let cycle = this.state.current_cycle;
        await this.getCycle(cycle + 1);
        await this.getCycle(cycle + 2);
        await this.getCycle(cycle + 3);
        await this.getCycle(cycle + 4);
        await this.getCycle(cycle + 5);
        this.setState({loading: true})
    }

    render() {
        const {current_cycle, current_baking_all, current_baking_miss, current_baking, current_endorse_all, current_endorse_miss, current_endorse, cycles} = this.state;
        if(this.state.loading) {
            return (
                <Container>
                    <Head sub={true} navigation={this.navigation} route={this.route}/>
                    <ScrollView style={{backgroundColor: '#1E2233'}}>
                        <Content style={{padding: 10, paddingTop: 0}}>
                            <View style={styles.header}><Text style={styles.h2}>Statistics of the current cycle</Text></View>
                            <View style={styles.block}>
                                <View style={[styles.tile, {minWidth: '35%'}]}>
                                    <Text style={styles.sub}>CYCLE</Text>
                                    <Text style={styles.text_tile}>{current_cycle}</Text>
                                </View>
                                <View style={[styles.tile, {minWidth: '45%'}]}>
                                    <Text style={styles.sub}>BAKING</Text>
                                    <Text style={styles.text_tile}>{current_baking-current_baking_miss}/{current_baking_all}</Text>
                                </View>
                                <View style={[styles.tile]}>
                                    <Text style={styles.sub}>ENDORSE</Text>
                                    <Text style={styles.text_tile}>{current_endorse-current_endorse_miss}/{current_endorse_all}</Text>
                                </View>
                            </View>
                            <View style={styles.header}><Text style={styles.h2}>Upcoming</Text></View>
                            <View style={styles.table}>
                                <View style={styles.thead}>
                                    <View style={styles.tr}>
                                        <Text style={[styles.th, {maxWidth: '20%', width: '100%'}]}>Cycle</Text>
                                        <Text style={[styles.th, {maxWidth: '40%', width: '100%'}]}>Bake</Text>
                                        <Text style={[styles.th, {maxWidth: '40%', width: '100%'}]}>Endorse</Text>
                                    </View>
                                </View>
                                <View style={styles.tbody}>
                                    {cycles.map((item) => {
                                        return (
                                            <View style={styles.tr}>
                                                <Text style={[styles.td, {maxWidth: '20%', width: '100%'}]}>{item.cycle}</Text>
                                                <Text style={[styles.td, {maxWidth: '39%', width: '100%'}]}>{item.baking}</Text>
                                                <Text style={[styles.td, {maxWidth: '40%', width: '100%'}]}>{item.endorse}</Text>
                                            </View>
                                        )
                                    })}
                                </View>
                            </View>
                        </Content>
                    </ScrollView>
                </Container>
            )
        } else {
            return (
                <Container>
                    <Head sub={true} navigation={this.navigation} route={this.route}/>
                    <ScrollView style={{backgroundColor: '#1E2233'}}>
                        <Content style={{padding: 10, paddingTop: 0}}>
                            <Spinner color='#319DFE' />
                        </Content>
                    </ScrollView>
                </Container>
            );
        }
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: normalize(26),
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block: {
        backgroundColor: '#121520',
        borderRadius: 20,
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding:8,
    },
    tile: {
        margin:8,
        backgroundColor: '#1E2233',
        borderRadius: 12,
        paddingTop: 20,
        paddingBottom: 15,
        paddingHorizontal:17,
        flex: 1,
        flexGrow: 1
    },
    sub: {
        fontFamily: "DINNextLTPro",
        fontSize: normalize(10),
        color: '#737B9C',
    },
    text_tile: {
        color: '#EAECF2',
        fontFamily: 'Circe_bold',
        fontSize: normalize(22)
    },
    table: {
        backgroundColor: '#121520',
        borderRadius: 20,
        overflow: 'hidden'
    },
    thead: {
        backgroundColor: '#0D0F17'
    },
    tr: {
        flex: 1,
        flexDirection: 'row'
    },
    th: {
        color: '#737B9C',
        fontFamily: 'DINNextLTPro',
        paddingTop: 12,
        paddingBottom: 7,
        textAlign: 'center',
        fontSize: normalize(18)
    },
    td: {
        fontFamily: 'Circe',
        fontSize: normalize(16),
        paddingVertical: 12,
        textAlign: 'center',
        color: '#EAECF2',
        flex: 1,
        alignItems: 'center'
    },
});
