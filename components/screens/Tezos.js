//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../Header';
import {SvgXml} from 'react-native-svg';
import { normalize } from '../normalize';

export default class Tezosscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          loading: false,
          tezos: {},
          tezos_update: true,
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    tezosUpdate() {
        this.setState({ tezos_update: false});
        fetch("https://api.tezos-nodes.com/v1/tezos")
        .then(response => response.json())
        .then(json => {
            this.setState({ tezos: json });
            this.setState({ tezos_update: true});
        })
    }

    number_format(number, decimals, dec_point, thousands_sep) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          var k = Math.pow(10, prec);
          return '' + (Math.round(n * k) / k)
            .toFixed(prec);
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '')
        .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
          .join('0');
      }
      return s.join(dec);
    }

    componentDidMount() {
        fetch("https://api.tezos-nodes.com/v1/tezos")
        .then(response => response.json())
        .then(json => {
            this.setState({ tezos: json })
            this.setState({ loading: true })
        })
    }
  render() {
      let { tezos } = this.state;
      const svgCycle = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="51" height="48" viewBox="0 0 51 48" fill="none"><path d="M39.6318 44.8945C44.8713 40.89 48.3111 34.629 48.4737 27.5232C48.6132 21.4253 46.3133 15.8386 42.4659 11.6969C38.4751 7.40072 32.8194 4.65924 26.4941 4.51455C14.071 4.23037 3.76963 14.071 3.48545 26.4941C3.31451 33.9667 6.8068 40.6716 12.3209 44.8906" stroke="#30DD36" stroke-dashoffset="0" stroke-width="6" stroke-linejoin="bevel" stroke-dasharray="110" /><path d="M39.6318 44.8945C44.8713 40.89 48.3111 34.629 48.4737 27.5232C48.6132 21.4253 46.3133 15.8386 42.4659 11.6969C38.4751 7.40072 32.8194 4.65924 26.4941 4.51455C14.071 4.23037 3.76963 14.071 3.48545 26.4941C3.31451 33.9667 6.8068 40.6716 12.3209 44.8906" stroke="#C4DFFF" stroke-dashoffset="' + tezos.cycle_percent*1.07 + '" stroke-width="6" stroke-linejoin="bevel" stroke-dasharray="110" /><path d="M39.6318 44.8945C44.8713 40.89 48.3111 34.629 48.4737 27.5232C48.6132 21.4253 46.3133 15.8386 42.4659 11.6969C38.4751 7.40072 32.8194 4.65924 26.4941 4.51455C14.071 4.23037 3.76963 14.071 3.48545 26.4941C3.31451 33.9667 6.8068 40.6716 12.3209 44.8906" stroke="#1E2233" stroke-width="7" stroke-linejoin="bevel" stroke-dasharray="2.5 1.5" /></svg>';
      const svgUpdate = '<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14 6.12497V0L11.9425 2.05623C10.6773 0.787498 8.93058 0 6.99561 0C3.1257 0 0 3.13248 0 7C0 10.8675 3.1257 14 6.99561 14C10.257 14 12.9887 11.7688 13.7636 8.75001H11.9425C11.2202 10.7888 9.28518 12.25 6.99565 12.25C4.09321 12.25 1.74237 9.90062 1.74237 7C1.74237 4.09938 4.09321 1.75001 6.99565 1.75001C8.44469 1.75001 9.74488 2.35376 10.6948 3.30315L7.87118 6.12502H14V6.12497Z" fill="#737B9C"/></svg>';

      if(tezos.percent_change_24h > 0) {
          var svgChange = '<svg width="6" height="4" viewBox="0 0 6 4" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6 4H0L3 0L6 4Z" fill="#30DD36"/></svg>';
          var changeColor = '#30DD36';
      } else {
           var svgChange = '<svg width="6" height="4" viewBox="0 0 6 4" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 0L6 0L3 4L0 0Z" fill="#F91525"/></svg>';
           var changeColor = '#F91525';
      }
      if(tezos.percent_change_btc_24h > 0) {
          var svgBtcChange = '<svg width="6" height="4" viewBox="0 0 6 4" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6 4H0L3 0L6 4Z" fill="#30DD36"/></svg>';
          var changeBtcColor = '#30DD36';
      } else {
          var svgBtcChange = '<svg width="6" height="4" viewBox="0 0 6 4" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 0L6 0L3 4L0 0Z" fill="#F91525"/></svg>';
          var changeBtcColor = '#F91525';
      }
      if(this.state.loading) {
          return (
        <Container>
<Head navigation={this.navigation} route={this.route} />
            <ScrollView style={{backgroundColor: '#1E2233'}}>
                <Content style={{padding: 10, paddingTop: 0}}>
                    <View style={[styles.header, {justifyContent: 'space-between'}]}>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={styles.h2}>
                                Tezos Network Status
                            </Text>
                        </View>
                        <TouchableOpacity onPress={() => this.tezosUpdate()}>
                            <SvgXml xml={svgUpdate} width="14" height="14" style={{padding: 7, marginRight: 10}} />
                        </TouchableOpacity>
                    </View>
                    {this.state.tezos_update ?
                        <View style={{flex: 1, flexDirection: 'column', flexWrap: 'nowrap', backgroundColor: '#121520', borderRadius: 20, padding: 8}}>
                           <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'column'}}>
                               <View style={{flex:1 , flexWrap: 'wrap', flexDirection: 'row'}}>
                                   <View style={[styles.tint_tezos, {flexGrow: 1, flexDirection: 'row', alignItems: 'center', maxWidth: '100%', minWidth: '90%'}]}>
                                       <View style={{marginTop: -5, marginLeft: -5, marginRight: 10}}>
                                           <SvgXml xml={svgCycle} width="45" height="40" />
                                           <Text style={{position: 'absolute', fontFamily: 'DINNextLTPro_bold', fontSize: normalize(12), left: 0, top: 16, right: 0, textAlign: 'center', color: '#30DD36'}}>{tezos.cycle_percent}%</Text>
                                       </View>
                                       <View>
                                           <Text style={styles.title}>CURRENT CYCLE</Text>
                                           <Text style={styles.text}>#{tezos.cycle} / -{tezos.time_value}</Text>
                                       </View>
                                   </View>
                                   <View style={[styles.tint_tezos, {flex: 1, flexGrow: 1, flexDirection: 'column', minWidth: '50%'}]}>
                                       <Text style={styles.title}>PRICE XTZ/USD</Text>
                                           <View>
                                               <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                                   <Text style={styles.text}>$ {this.state.tezos.tezos_price_usd}</Text>
                                                   <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 5}}>
                                                       <Text style={{fontFamily: 'Circe_bold', fontSize: normalize(12), color: changeColor, marginRight: 4}}>{tezos.percent_change_24h}</Text>
                                                       <SvgXml xml={svgChange} />
                                                   </View>
                                               </View>
                                           </View>
                                   </View>
                                   <View style={[styles.tint_tezos, {flex: 1, flexGrow: 1, flexDirection: 'column', minWidth: '35%'}]}>
                                       <Text style={styles.title}>PUBLIC BAKERS</Text>
                                       <Text style={styles.text}>{tezos.public_bakers}</Text>
                                   </View>
                                   <View style={[styles.tint_tezos, {flex: 1, flexGrow: 1, flexDirection: 'column', minWidth: '50%'}]}>
                                       <Text style={styles.title}>PRICE XTZ/BTC</Text>
                                       <View>
                                           <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                               <Text style={styles.text}>{this.state.tezos.tezos_price_btc}</Text>
                                               <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: 5}}>
                                                   <Text style={{fontFamily: 'Circe_bold', fontSize: normalize(12), color: changeBtcColor, marginRight: 4}}>{tezos.percent_change_btc_24h}</Text>
                                                   <SvgXml xml={svgBtcChange} />
                                               </View>
                                           </View>
                                       </View>
                                   </View>
                                   <View style={[styles.tint_tezos, {flex: 1, flexGrow: 1, flexDirection: 'column', minWidth: '35%'}]}>
                                       <Text style={styles.title}>ACTIVE BAKERS</Text>
                                       <Text style={styles.text}>{tezos.active_bakers}</Text>
                                   </View>
                               </View>
                           </View>
                           <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'column'}}>
                               <View style={{flex:1 , flexWrap: 'wrap', flexDirection: 'row'}}>
                                   <View style={[styles.tint_tezos, {flex: 1, flexGrow: 1, flexDirection: 'column', minWidth: '50%'}]}>
                                       <Text style={styles.title}>MARKET CAP</Text>
                                       <Text style={styles.text}>$ {this.number_format(tezos.market_cap, 0, '.', ',')}</Text>
                                   </View>
                                   <View style={[styles.tint_tezos, {flex: 1, flexGrow: 1, flexDirection: 'column', minWidth: '35%'}]}>
                                       <Text style={styles.title}>ACTIVE DELEGATORS</Text>
                                       <Text style={styles.text}>{this.number_format(tezos.active_delegators, '.', ',')}</Text>
                                   </View>
                                   <View style={[styles.tint_tezos, {flex: 1, flexGrow: 1, flexDirection: 'column', minWidth: '50%'}]}>
                                       <Text style={styles.title}>CIRCULATING SUPPLY</Text>
                                       <Text style={styles.text}>{tezos.circulating_supply}</Text>
                                   </View>
                                   <View style={[styles.tint_tezos, {flex: 1, flexGrow: 1, flexDirection: 'column', minWidth: '35%'}]}>
                                       <Text style={styles.title}>STAKING RATIO</Text>
                                       <Text style={styles.text}>{tezos.staking_ratio}%</Text>
                                   </View>
                               </View>
                           </View>
                        </View>
                        :
                        <Spinner color='#319DFE' />
                    }
                </Content>
            </ScrollView>
        </Container>
    ) } else {
        return (
                   <Container>
                      <Head navigation={this.navigation} route={this.route} />
                       <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                           <Spinner color='#319DFE' />
                       </Content>
                   </Container>
        )
    }
  }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: normalize(26),
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    tint_tezos: {
        flex: 1,
        flexGrow: 1,
        minWidth: '40%',
        margin: 8,
        backgroundColor: '#1E2233',
        borderRadius: 12,
        paddingTop: 19,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 10
    },
    tints: {

    },
    title: {
        fontFamily: "DINNextLTPro",
        fontSize: normalize(10),
        color: '#737B9C',
    },
    text: {
        fontFamily: "Circe_bold",
        fontSize: normalize(22),
        color: '#EAECF2',
    },
});