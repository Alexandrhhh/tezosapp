//Homescreen.js

import React, { Component, useState } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Linking, AsyncStorage, Alert} from 'react-native';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Toast, Spinner} from 'native-base';
import {SvgXml} from 'react-native-svg';
import Head from '../Header';
import * as WebBrowser from 'expo-web-browser';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import { normalize } from '../normalize';

export default class Calcscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            authorized: false,
            login: '',
            password: '',
            showToast: false,
            token: '',
            token_type: '',
            cabinet: false,
            baker_address: '',
            verify: false,
            data: [],
            input_baker: ''
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    async openUrl(uri) {
        await WebBrowser.openBrowserAsync(uri);
    }

    login() {
        let user = {
            email: this.state.login,
            password: this.state.password
        }
        fetch('https://api.tezos-nodes.com/api/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            },
            body: 'email=' + user.email + '&password=' + user.password
        })
        .then(response => response.json())
        .then(json => {
            if(json.error) {
                Toast.show({
                  text: 'The username or password is incorrect',
                  textStyle: {textAlign: 'center'},
                  type: 'danger',
                  duration: 2000
                })
            } else {
                Toast.show({
                  text: 'Authorization was successful!',
                  textStyle: {textAlign: 'center'},
                  type: 'success',
                  duration: 2000
                })
                this.setState({authorized: true, token: json.access_token, token_type: json.token_type})
                AsyncStorage.setItem('token', json.access_token);
                AsyncStorage.setItem('token_type', json.token_type);
                this.authorized();
            }
        })
    }

    logout() {
        AsyncStorage.removeItem('token')
        AsyncStorage.removeItem('token_type')
        this.setState({token: '', token_type: '', authorized: false, cabinet: false, baker_address: ''})
    }

    authorized() {
        AsyncStorage.getItem('token')
        .then((token) => {
            if(token !== null) {
                this.setState({authorized: true, token: token})
                fetch('https://api.tezos-nodes.com/api/auth/me', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json',
                        'Authorization': this.state.token_type + " " + this.state.token
                    }
                })
                .then(response => response.json())
                .then(json => {
                    if(json.baker_address !== '' && json.baker_address !== null) {
                        this.setState({cabinet: true, baker_address: json.baker_address})
                    } else {
                        this.setState({cabinet: false, baker_address: ''})
                    }
                })
            } else {
                this.setState({authorized: false})
            }
        })
        AsyncStorage.getItem('token_type')
        .then((token_type) => {
            if(token_type !== null) {
                this.setState({token_type: token_type})
            }
        })
    }

    verify() {
        if(this.state.input_baker.length !== 36) {
            Alert.alert('Error', 'The search must be 36 characters')
        } else {
            this.setState({verify: true})
            fetch('https://api.tezos-nodes.com/api/auth/verify', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json',
                    'Authorization': this.state.token_type + " " + this.state.token
                },
                body: 'search=' + this.state.input_baker
            })
            .then(response => response.json())
            .then(json => {
                if(json.error) {
                    Toast.show({
                      text: json.error,
                      textStyle: {textAlign: 'center'},
                      type: 'danger',
                      duration: 3000
                    })
                    this.setState({verify: false})
                } else {
                    this.setState({data: json})
                }
            })
        }
    }

    componentDidMount() {
        this.setState({loading: true});
        this.authorized();
        this._unsubscribe = this.navigation.addListener('focus', () => {
          this.authorized();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

  render() {
      const svgAccount = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="25" height="25" viewBox="0 0 25 25" fill="none"><path d="M0 12.5C0 5.59139 5.59063 0 12.5 0C19.4086 0 25 5.59063 25 12.5C25 19.3474 19.4561 25 12.5 25C5.57251 25 0 19.3789 0 12.5ZM12.5 1.46484C6.41518 1.46484 1.46484 6.41518 1.46484 12.5C1.46484 14.7259 2.12688 16.8653 3.35464 18.6775C8.28476 13.3743 16.7063 13.3648 21.6454 18.6775C22.8731 16.8653 23.5352 14.7259 23.5352 12.5C23.5352 6.41518 18.5848 1.46484 12.5 1.46484ZM20.7317 19.8486C16.3483 14.9319 8.65059 14.933 4.26846 19.8486C8.66661 24.7692 16.3317 24.7707 20.7317 19.8486Z" fill="#3E4257"/><path d="M12.5 13.2324C10.0769 13.2324 8.10547 11.261 8.10547 8.83789V7.37305C8.10547 4.94995 10.0769 2.97852 12.5 2.97852C14.9231 2.97852 16.8945 4.94995 16.8945 7.37305V8.83789C16.8945 11.261 14.9231 13.2324 12.5 13.2324ZM15.4297 7.37305C15.4297 5.75752 14.1155 4.44336 12.5 4.44336C10.8845 4.44336 9.57031 5.75752 9.57031 7.37305V8.83789C9.57031 10.4534 10.8845 11.7676 12.5 11.7676C14.1155 11.7676 15.4297 10.4534 15.4297 8.83789V7.37305Z" fill="#3E4257"/></svg>';
      const svgPassword = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="22" height="25" viewBox="0 0 22 25" fill="none"><path d="M16.5039 17.8711C17.0432 17.8711 17.4805 17.4339 17.4805 16.8945C17.4805 16.3552 17.0432 15.918 16.5039 15.918C15.9646 15.918 15.5273 16.3552 15.5273 16.8945C15.5273 17.4339 15.9646 17.8711 16.5039 17.8711Z" fill="#3E4257"/><path d="M16.6951 9.17969V9.27969H16.7951H17.9688C20.0674 9.27969 21.775 10.9873 21.775 13.0859V16.6992C21.775 17.1833 21.3826 17.5758 20.8984 17.5758C20.4143 17.5758 20.0219 17.1833 20.0219 16.6992V13.0859C20.0219 11.9538 19.1009 11.0328 17.9688 11.0328H3.90625C2.77407 11.0328 1.85312 11.9538 1.85312 13.0859V21.0938C1.85312 22.2259 2.77407 23.1469 3.90625 23.1469H17.9688C19.1009 23.1469 20.0219 22.2259 20.0219 21.0938C20.0219 20.6096 20.4143 20.2172 20.8984 20.2172C21.3826 20.2172 21.775 20.6096 21.775 21.0938C21.775 23.1924 20.0674 24.9 17.9688 24.9H3.90625C1.80757 24.9 0.1 23.1924 0.1 21.0938V13.0859C0.1 10.9873 1.80757 9.27969 3.90625 9.27969H5.07632H5.17632V9.17969V5.73564C5.17632 2.63023 7.75804 0.1 10.9357 0.1C14.1133 0.1 16.6951 2.63023 16.6951 5.73564V9.17969ZM14.8419 9.27969H14.9419V9.17969V5.73564C14.9419 3.59171 13.1418 1.85312 10.9357 1.85312C8.72962 1.85312 6.92944 3.59171 6.92944 5.73564V9.17969V9.27969H7.02944H14.8419Z" fill="#3E4257" stroke="#090A11" stroke-width="0.2"/><path d="M9.13086 17.8711C9.6702 17.8711 10.1074 17.4339 10.1074 16.8945C10.1074 16.3552 9.6702 15.918 9.13086 15.918C8.59152 15.918 8.1543 16.3552 8.1543 16.8945C8.1543 17.4339 8.59152 17.8711 9.13086 17.8711Z" fill="#3E4257"/><path d="M5.46875 17.8711C6.00809 17.8711 6.44531 17.4339 6.44531 16.8945C6.44531 16.3552 6.00809 15.918 5.46875 15.918C4.92941 15.918 4.49219 16.3552 4.49219 16.8945C4.49219 17.4339 4.92941 17.8711 5.46875 17.8711Z" fill="#3E4257"/><path d="M12.793 17.8711C13.3323 17.8711 13.7695 17.4339 13.7695 16.8945C13.7695 16.3552 13.3323 15.918 12.793 15.918C12.2536 15.918 11.8164 16.3552 11.8164 16.8945C11.8164 17.4339 12.2536 17.8711 12.793 17.8711Z" fill="#3E4257"/></svg>';

    if(this.state.loading) {
        if(this.state.authorized) {
            let {baker_address} = this.state;
            return (
                <Container>
                <Head navigation={this.navigation} route={this.route} />
                    <ScrollView style={{backgroundColor: '#1E2233'}}>
                        <Content style={{padding: 10, paddingTop: 15}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginHorizontal: -7.5, marginBottom: 15}}>
                                {this.state.cabinet ? <Button style={[styles.btn, styles.btn_edit]} onPress={() => this.navigation.navigate('Cabinet', {time: JSON.stringify(new Date()), token: this.state.token, token_type: this.state.token_type})}><Text style={styles.btn_edit_text}>Baker edit</Text></Button> : <Text></Text>}
                                <Button style={[styles.btn, styles.btn_logout]} onPress={() => this.logout()}><Text style={styles.btn_logout_text}>Logout account</Text></Button>
                            </View>
                            <View style={styles.info_block}>
                                <Text style={styles.info_block_text}>Hello dear user of Tezos Nodes, the reliability rating service of bakers in the Tezos network. We thank you for your interest in our project. You can now easily identify the efficiency of any baker as well as the free space they have for delegations.</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 40, marginBottom: 20, marginHorizontal: -7.5}}>
                                <TextInput style={[styles.input_baker, {marginHorizontal: 7.5}]} maxLength={36} placeholder="Baker address Tz..." placeholderTextColor="#737B9C" value={this.state.input_baker} onChangeText={(text) => this.setState({input_baker: text})}></TextInput>
                                <Button style={[styles.btn, styles.btn_edit, {height: 48, maxWidth: 111}]} onPress={() => this.verify()}><Text style={styles.btn_edit_text}>Verify</Text></Button>
                            </View>
                            {this.state.verify && this.state.data.length == 0 ?
                                <Spinner color='#319DFE' />
                                :
                                this.state.data.length != 0 ?
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                    <View style={styles.table}>
                                        <View style={styles.thead}>
                                            <View style={styles.tr}>
                                                <Text style={[styles.th, {width: 100}]}>Date</Text>
                                                <Text style={[styles.th, {width: 70}]}>Time</Text>
                                                <Text style={[styles.th, {width: 350}]}>Baking address</Text>
                                                <Text style={[styles.th, {width: 90}]}>Lifetime</Text>
                                                <Text style={[styles.th, {width: 95}]}>Efficiency</Text>
                                                <Text style={[styles.th, {width: 90}]}>Last 10</Text>
                                                <Text style={[styles.th, {width: 100}]}>Free capacity</Text>
                                            </View>
                                        </View>
                                        <View style={styles.tbody}>
                                            <GestureHandlerScrollView>
                                                {this.state.data.map((baker, i) => {
                                                    let date = new Date(baker.created_at);
                                                    return (<View style={styles.tr_tbody} key={i}>
                                                        <Text style={[styles.td, {width: 100}]}>{date.getDate() < 10 ? '0' + date.getDate() : date.getDate()}.{date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth()}.{date.getFullYear()}</Text>
                                                        <Text style={[styles.td, {width: 70}]}>{date.getHours() < 10 ? '0' + date.getHours() : date.getHours()}:{date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()}</Text>
                                                        <Text style={[styles.td, {width: 350}]}>{baker.baker_address}</Text>
                                                        <Text style={[styles.td, {width: 90}]}>{baker.lifetime}</Text>
                                                            <View style={[styles.td, {width: 95}]}>
                                                                <View style={[styles.tint, baker.efficiency_all > 96 ? styles.green : baker.efficiency_all > 90 ? styles.yellow : styles.red]}>
                                                                    <Text style={[styles.tint_text, {textAlign: 'center'}, , baker.efficiency_all > 96 ? styles.green : baker.efficiency_all > 90 ? styles.yellow : styles.red]}>{baker.efficiency_all}%</Text>
                                                                </View>
                                                            </View>
                                                            <View style={[styles.td, {width: 90}]}>
                                                                <View style={[styles.tint, baker.efficiency_last10cycle > 96 ? styles.green : baker.efficiency_last10cycle > 90 ? styles.yellow : styles.red]}>
                                                                    <Text style={[styles.tint_text, {textAlign: 'center', flexWrap: 'nowrap'}, , baker.efficiency_last10cycle > 96 ? styles.green : baker.efficiency_last10cycle > 90 ? styles.yellow : styles.red]}> {baker.efficiency_last10cycle}%</Text>
                                                                </View>
                                                            </View>
                                                        <Text style={[styles.td, {width: 100}]}>{baker.freespace > 1000000 ? (baker.freespace/1000000).toFixed(2) + 'kk' : (baker.freespace/1000).toFixed(2) + 'k'}</Text>
                                                    </View>)
                                                })}
                                            </GestureHandlerScrollView>
                                        </View>
                                    </View>
                                </ScrollView>
                                :
                                <Text></Text>
                            }
                        </Content>
                    </ScrollView>
                </Container>
            );
        } else {
            return (
                <Container>
                <Head navigation={this.navigation} route={this.route} />
                    <ScrollView style={{backgroundColor: '#1E2233'}}>
                        <Content style={{padding: 10, paddingTop: 0}}>
                            <View style={styles.header}>
                                <Text style={styles.h2}>Log In</Text>
                            </View>
                            <View style={styles.login_form}>
                                <View>
                                   <SvgXml xml={svgAccount} style={styles.form_icon} width="25" height="25"/>
                                   <TextInput style={styles.login_form_input} placeholder="Your Login" value={this.state.login} onChangeText={(text) => this.setState({login: text})} placeholderTextColor={'#737B9C'}></TextInput>
                                </View>
                                <View>
                                    <SvgXml xml={svgPassword} style={styles.form_icon} width="25" height="25"/>
                                   <TextInput style={styles.login_form_input} placeholder="Your Password" value={this.state.password} onChangeText={(text) => this.setState({password: text})} placeholderTextColor={'#737B9C'} secureTextEntry={true}></TextInput>
                                </View>
                                <TouchableOpacity onPress={() => this.openUrl('https://tezos-nodes.com/password/reset')}>
                                    <Text style={{color: '#319DFE', fontFamily: 'Circe', fontSize: normalize(14), textAlign: 'right', marginBottom: 15}}>Forgot your password?</Text>
                                </TouchableOpacity>
                                <View style={styles.login_buttons}>
                                    <Button style={styles.login_button} onPress={() => this.login()}><Text style={styles.login_text}>Login</Text></Button>
                                </View>
                                <Text style={{fontFamily: 'Circe', fontSize: normalize(16), color: '#EAECF2', textAlign: 'center', marginTop: 20}}>Don’t have an account?</Text>
                                <View style={styles.login_buttons}>
                                    <Button onPress={() => this.openUrl('https://tezos-nodes.com/register')} style={styles.register_button}><Text style={styles.register_text}>{this.state.authorized ? 'Logout' : 'Register'}</Text></Button>
                                </View>
                            </View>
                        </Content>
                    </ScrollView>
                </Container>
            )
        }
    } else {
        return (
           <Container>
      <Head navigation={this.navigation} route={this.route} />
               <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                   <Spinner color='#319DFE' />
               </Content>
           </Container>
        )
    }
  }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: normalize(26),
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    login_form: {
        backgroundColor: '#121520',
        borderRadius: 20,
        padding: 16,
    },
    login_form_input: {
        backgroundColor: '#090A11',
        borderRadius: 12,
        marginBottom: 15,
        paddingLeft: 50,
        height: 60,
        color: '#737B9C',
        fontFamily: 'Circe',
        fontSize: normalize(16),
    },
    form_icon: {
        position: 'absolute',
        top: 17,
        left: 14,
        zIndex: 1
    },
    login_buttons: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    login_button: {
        flexGrow: 1,
        backgroundColor: '#319DFE',
        height: 60,
        borderRadius: 12,
        flex: 1,
        justifyContent: 'center',
        marginBottom: 7
    },
    login_text: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(16),
        color: 'white'
    },
    register_button: {
        flexGrow: 1,
        backgroundColor: 'transparent',
        height: 60,
        borderRadius: 12,
        flex: 1,
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#333952',
        marginBottom: 7
    },
    register_text: {
        color: '#6F7693',
        fontFamily: 'Circe_bold',
        fontSize: normalize(16),
    },
    btn: {
        flexGrow: 1,
        flex: 1,
        margin: 7.5,
        height: 48,
        borderRadius: 12,
        justifyContent: 'center'
    },
    btn_edit: {
        backgroundColor: '#319DFE'
    },
    btn_edit_text: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(16),
        lineHeight: 38,
        color: '#FFFFFF',
    },
    btn_logout: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: '#333952'
    },
    btn_logout_text: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(16),
        lineHeight: 38,
        color: '#6F7693',
    },
    info_block: {
        padding: 25,
        borderRadius: 20,
        backgroundColor: '#121520',
    },
    info_block_text: {
        fontFamily: 'Circe',
        fontSize: normalize(16),
        lineHeight: 22,
        color: '#EAECF2'
    },
    input_baker: {
        flex: 1,
        flexGrow: 1,
        backgroundColor: '#090A11',
        borderRadius: 12,
        color: '#737B9C',
        fontFamily: 'Circe',
        fontSize: normalize(16),
        height: 48,
        paddingHorizontal: 29
    },
    table: {
        borderRadius: 20,
        overflow: 'hidden'
    },
    thead: {
        backgroundColor: '#0D0F17'
    },
    tbody: {
        backgroundColor: '#121520',
        maxHeight: 350
    },
    tr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 58,
    },
    tr_tbody: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 62,
        borderBottomWidth: 1,
        borderBottomColor: '#1E212B'
    },
    th: {
        color: '#737B9C',
        fontFamily: 'DINNextLTPro',
        fontSize: normalize(16),
        paddingLeft: 20
    },
    td: {
        color: '#EAECF2',
        fontFamily: 'Circe',
        fontSize: normalize(16),
        paddingLeft: 20
    },
    tint_text: {
        fontFamily: 'Circe',
        fontSize: normalize(14),
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 4,
        paddingBottom: 2,
    },
    green: {
        color: '#30DD36',
        borderColor: '#30DD36'
    },
    red: {
        color: '#F91525',
        borderColor: '#F91525'
    },
    yellow: {
        color: '#FFAD15',
        borderColor: '#FFAD15'
    },
});
