//Homescreen.js

import React, { Component } from 'react';
import { View, StyleSheet, Image, ScrollView, TouchableOpacity, AsyncStorage} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../Header';
import { normalize } from '../normalize';

export default class Newsscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newses: [],
            loading: false,
            reading: []
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    async updateNews() {
        await fetch('https://api.tezos-nodes.com/v1/news')
        .then(response => response.json())
        .then(json => {
            this.setState({ newses: json })
            this.setState({ loading: true })
        })
        await AsyncStorage.getItem('reading')
        .then(reading => {
            if(reading !== null) {
                this.setState({reading: JSON.parse(reading)})
            }
        })
    }

    componentWillUnmount() {
        this._update();
    }

    componentDidMount() {
        this._update = this.navigation.addListener('focus', () => {
          this.updateNews();
        });
        this.updateNews();
    }
  render() {
      let {newses} = this.state;
      if(this.state.loading) {
          return (
              <Container>
            <Head navigation={this.navigation} route={this.route} />
                  <ScrollView style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                      <Content style={{padding: 10, paddingTop: 0}}>
                          <View style={styles.header}><Text style={styles.h2}>News</Text></View>
                          {newses.map((news, i) => (
                              <View style={styles.block_news} key={news.id}>
                                  <View style={styles.news_image_block}>
                                      <Image source={{uri: news.image}} style={styles.news_image}/>
                                  </View>
                                  {(this.state.reading.length == 0 || this.state.reading.indexOf(news.id) == -1) ?
                                      <View style={{flex: 1, alignItems: 'center'}}><View style={{borderRadius: 10, overflow: 'hidden', marginBottom: 10, marginTop: -10}}><Text style={styles.badge}>NEW</Text></View></View>
                                      :
                                      <View></View>
                                    }
                                  <Text style={styles.news_title}>{news.title}</Text>
                                  <Button style={[styles.news_button, {width: '100%'}]} onPress={() => this.navigation.navigate('PersonalNews', {id: news.id})}><Text style={styles.news_button_text}>Read more</Text></Button>
                              </View>
                          ))}
                      </Content>
                  </ScrollView>
              </Container>
          )
      } else {
          return (
             <Container>
          <Head navigation={this.navigation} route={this.route} />
                 <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                     <Spinner color='#319DFE' />
                 </Content>
             </Container>
          )
      }
  }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: normalize(26),
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_news: {
        backgroundColor: '#121520',
        borderRadius: 20,
        padding: 16,
        marginBottom: 30
    },
    news_image_block: {
        paddingTop: '100%',
        width: '100%',
        borderRadius: 12,
        overflow: 'hidden',
        marginBottom: 20,
    },
    news_image: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        flex: 1,
        width: null,
        height: null,
        resizeMode:'cover',
    },
    news_title: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(20),
        lineHeight: 28,
        color: '#EAECF2',
        marginBottom: 20,
        flex: 1
    },
    news_content: {
        fontFamily: 'Circe',
        fontSize: normalize(18),
        color: '#737B9C',
        lineHeight: 22,
        marginBottom: 38
    },
    news_button: {
        height: 60,
        borderWidth: 1,
        borderColor: '#333952',
        borderRadius: 12,
        backgroundColor: 'transparent',
        flex: 1,
        justifyContent: 'center'
    },
    news_button_text: {
        color: '#6F7693',
        fontFamily: 'Circe_bold',
        fontSize: normalize(16)
    },
    badge: {
        borderRadius: 30,
        backgroundColor: '#319DFE',
        paddingVertical: 5,
        paddingHorizontal: 10,
        fontSize: normalize(10),
        fontFamily: 'Circe_bold',
        color: '#ffffff',
        lineHeight: 15,
        letterSpacing: 1.3
    }

});