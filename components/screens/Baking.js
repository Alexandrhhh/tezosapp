import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TextInput} from 'react-native';
import {Container, Content, Button, Text, Spinner, Toast} from 'native-base';
import Head from '../Header';
import { normalize } from '../normalize';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import moment from "moment";

export default class BakingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            seconds: new Date().getSeconds(),
            number: 0,
            cycle: 0,
            time: null,
            view: false,
            address: props.route.params !== undefined && props.route.params.address !== undefined ? props.route.params.address : '',
            next: {
                titles: [],
                objects: []
            },
            prev: {
                titles: [],
                objects: []
            },
            subLoad: false
        }
        this.loading = true;
        this.navigation = props.navigation;
        this.route = props.route;
        if(this.state.address.length == 36) {
            this.getRequest();
        }
    }

    componentDidMount() {
        this.runSecond();
        this.getHeight();
    }

    view() {
        this.setState({view: true})
    }

    getHeight() {
        fetch('https://api.tzstats.com/explorer/tip')
            .then(response => response.json())
            .then(json => {
                if(json.height < this.state.number) {
                    this.setState({cycle: json.cycle, time: moment(json.timestamp)})
                } else {
                    this.setState({number: json.height, cycle: json.cycle, time: moment(json.timestamp)})
                }
                this.setState({cycle: json.cycle, time: moment(json.timestamp)})
            })
    }

    async getBacking() {
        let {cycle, address, number} = this.state;
        let url = {
            prevBaking: 'https://api.tzstats.com/tables/rights?address=' + address + '&cycle.rg=' + (cycle - 5) + ',' + (cycle + 5) + '&limit=5&height.lt=' + number + '&priority=0&type=baking&columns=row_id,type,height,is_missed,time&order=desc',
            nextBaking: 'https://api.tzstats.com/tables/rights?address=' + address + '&cycle.rg=' + (cycle - 5) + ',' + (cycle + 5) + '&limit=5&height.gte=' + number + '&priority=0&type=baking&columns=row_id,type,height,is_missed,time',
            prevEndorse: 'https://api.tzstats.com/tables/rights?address=' + address + '&cycle.rg=' + (cycle - 5) + ',' + (cycle + 5) + '&limit=5&height.lt=' + number + '&type=endorsing&columns=row_id,type,height,is_missed,time&order=desc',
            nextEndorse: 'https://api.tzstats.com/tables/rights?address=' + address + '&cycle.rg=' + (cycle - 5) + ',' + (cycle + 5) + '&limit=5&height.gte=' + number + '&type=endorsing&columns=row_id,type,height,is_missed,time',
        }
        let prev = [], next = [];
        await fetch(url.prevBaking)
            .then((response) => response.json())
            .then((json) => {
                json.map((item) => {
                    let element = {
                        id: item[0],
                        height: item[2],
                        type: item[1],
                        miss: item[3],
                        time: moment(item[4])
                    }
                    prev.push(element);
                })
            })
            .catch((error) => {
                Toast.show({
                    text: 'Baker not found',
                    textStyle: {textAlign: 'center'},
                    type: 'danger',
                    duration: 2000
                })
                this.setState({view: 0})
            });
        await fetch(url.nextBaking)
            .then((response) => response.json())
            .then((json) => {
                json.map((item) => {
                    let element = {
                        id: item[0],
                        height: item[2],
                        type: item[1],
                        miss: item[3],
                        time: moment(item[4])
                    }
                    next.push(element);
                })
            })
        await fetch(url.prevEndorse)
            .then((response) => response.json())
            .then((json) => {
                json.map((item) => {
                    let element = {
                        id: item[0],
                        height: item[2],
                        type: item[1],
                        miss: item[3],
                        time: moment(item[4])
                    }
                    prev.push(element);
                })
            })
        await fetch(url.nextEndorse)
            .then((response) => response.json())
            .then((json) => {
                json.map((item) => {
                    let element = {
                        id: item[0],
                        height: item[2],
                        type: item[1],
                        miss: item[3],
                        time: moment(item[4])
                    }
                    next.push(element);
                })
                this.view();
            })
        next.sort((a, b) => {
            return a.height - b.height;
        })

        next = this.groupBy(next)

        let titles = [];

        for (let title in next) {
            if (next.hasOwnProperty(title)) {
                titles.push(title)
            }
        }
        this.setState({
            next: {titles: titles.slice(0, 5), objects: next}
        })

        prev = this.groupBy(prev)

        let titles2 = [];

        for (let title in prev) {
            if (prev.hasOwnProperty(title)) {
                titles2.push(title)
            }
        }

        titles2.sort((a, b) => {
            return b - a;
        })

        this.setState({
            prev: {titles: titles2.slice(0, 5), objects: prev}
        })
    }

    groupBy = (items, key) => items.reduce(
        (result, item) => ({
            ...result,
            [item.height]: [
                ...(result[item.height] || []),
                item,
            ],
        }),
        {},
    );

    getRequest() {
        if(this.state.address.length != 36) {
            Toast.show({
                text: 'The search must be 36 characters',
                textStyle: {textAlign: 'center'},
                type: 'danger',
                duration: 2000
            })
        } else {
            this.getHeight();
            this.getBacking();
        }
    }

    runSecond() {
        setInterval(() => {
            if(Math.floor(moment().diff(this.state.time)) < 30) {
                this.setState({seconds: Math.floor(moment().diff(this.state.time) / 1000)})
            } else {
                if(this.state.seconds >= 29) {
                    this.setState({seconds: this.state.seconds+1-30})
                    this.setState({number: this.state.number+1})
                } else {
                    this.setState({seconds: this.state.seconds+1})
                }
            }

        }, 1000)
        setInterval(() => {
            if(this.state.view) {
                this.getBacking()
                this.getHeight()
            }
        }, 5000)
    }

    number_format(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                    .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
            .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
                .join('0');
        }
        return s.join(dec);
    }

    objSize(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    getTime(height) {
        let i, k;
        if(height < this.state.number) {
            i = this.state.number - height;
            return (<Text>i</Text>);
        } else {
            i = this.state.number - height;
            return (<Text>i</Text>);
        }
    }

    tr_rander(array) {
        let d = array.titles.map((title, i) => {
            let bake = 0, endorse = 0, endorsefailed = 0, bakefailed = 0;
            array.objects[title].map((object) => {
                if(object.type == 'endorsing') {
                    endorse++;
                    if(object.miss) {
                        endorsefailed++;
                    }
                } else {
                    bake++;
                    if(object.miss) {
                        bakefailed++;
                    }
                }
            })
            return (
                <View style={styles.tr} key={array.objects[title][0].id}>
                    <Text style={[styles.td, {maxWidth: '10%', width: '100%'}]}>{i+1}</Text>
                    <View style={[styles.td, {maxWidth: '40%', width: '100%'}]}>
                        <Text style={styles.opacity0}>{title}</Text>
                        <Text style={[styles.opacity0, styles.opacity5]}>{array.objects[title][0].time.fromNow()}</Text>
                    </View>
                    <Text style={[styles.td, {maxWidth: '20%', width: '100%'}]}>
                        {this.state.number < array.objects[title][0].height ?
                            <Text style={styles.td}>{bake ? '0/'+bake : ''}</Text>
                            :
                            <Text style={bakefailed ? styles.red : styles.green}>{bake > 0 ? bake - bakefailed + '/' + bake : ''}</Text>
                        }
                    </Text>
                    <Text style={[styles.td, {maxWidth: '30%', width: '100%'}]}>
                        {this.state.number < array.objects[title][0].height ?
                            <Text style={styles.td}>{endorse ? '0/'+endorse : ''}</Text>
                            :
                            <Text style={endorsefailed ? styles.red : styles.green}>{endorse > 0 ? endorse - endorsefailed + '/' + endorse : ''}</Text>
                        }
                    </Text>
                </View>
            )
        })
        return d;
    }

    render() {
        const {seconds, number, view, address, subLoad, next, prev} = this.state;
        if(this.loading) {
            return (
                <Container>
                <Head navigation={this.navigation} route={this.route} />
                    <ScrollView style={{backgroundColor: '#1E2233'}}>
                        <Content style={{padding: 10, paddingTop: 0}}>
                            <View style={styles.header}>
                                <Text style={styles.h2}>
                                    Baking control
                                </Text>
                            </View>
                            <View style={styles.block_info}>
                                <Text style={styles.text}>Enter a Tezos Baker's address to view rights baking blocks and endorsers</Text>
                            </View>
                                <View style={styles.block_info}>
                                    <TextInput style={ styles.input } placeholder="Baking address Tz1BaKi4gAddRes8…" maxLength={36} value={this.state.address} onChangeText={(text) => this.setState({address: text})} placeholderTextColor={ '#737B9C' }/>
                                    <Button style={styles.button} onPress={() => this.getRequest()}><Text style={styles.button_text}>Verify</Text></Button>
                                </View>
                            {view ?
                                <View>
                                    <View style={[styles.block_info, styles.circ_bg]}>
                                        <AnimatedCircularProgress
                                            size={174}
                                            width={5}
                                            backgroundWidth={5}
                                            fill={100/30*seconds}
                                            tintColor="#319DFE"
                                            tintColorSecondary="#319DFE"
                                            backgroundColor="#000"
                                            arcSweepAngle={360}
                                            rotation={360}
                                            lineCap="round"
                                            style={{marginVertical: 20}}
                                        >
                                            {fill => <View>
                                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                                    <Text style={styles.circ_text}>{seconds}</Text>
                                                    <Text style={styles.circ_sub}>Updated</Text>
                                                </View>
                                            </View>}
                                        </AnimatedCircularProgress>
                                        <View style={styles.circ_block}>
                                            <Text style={styles.circ_hash}>#</Text>
                                            <Text style={styles.circ_number}>{this.number_format(number, '', '', '.')}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.header}>
                                        <Text style={styles.h2}>Upcoming</Text>
                                    </View>
                                    <View style={styles.table}>
                                        <View style={styles.thead}>
                                            <View style={styles.tr}>
                                                <Text style={[styles.th, {maxWidth: '10%', width: '100%', textAlign: 'center'}]}>#</Text>
                                                <Text style={[styles.th, {maxWidth: '40%', width: '100%'}]}>Block time</Text>
                                                <Text style={[styles.th, {maxWidth: '20%', width: '100%'}]}>Bake</Text>
                                                <Text style={[styles.th, {maxWidth: '30%', width: '100%'}]}>Endorse</Text>
                                            </View>
                                        </View>
                                        <View style={styles.tbody}>
                                            {Object.keys(next.objects).length ?
                                            this.tr_rander(next) : <Spinner color='#319DFE' />
                                            }
                                        </View>
                                    </View>
                                    <View style={styles.header}>
                                        <Text style={styles.h2}>Completed</Text>
                                    </View>
                                    <View style={styles.table}>
                                        <View style={styles.thead}>
                                            <View style={styles.tr}>
                                                <Text style={[styles.th, {maxWidth: '10%', width: '100%', textAlign: 'center'}]}>#</Text>
                                                <Text style={[styles.th, {maxWidth: '40%', width: '100%'}]}>Block time</Text>
                                                <Text style={[styles.th, {maxWidth: '20%', width: '100%'}]}>Bake</Text>
                                                <Text style={[styles.th, {maxWidth: '30%', width: '100%'}]}>Endorse</Text>
                                            </View>
                                        </View>
                                        <View style={styles.tbody}>
                                            {Object.keys(prev.objects).length ?
                                                this.tr_rander(prev) : <Spinner color='#319DFE' />
                                            }
                                        </View>
                                    </View>
                                    <Button style={[styles.button, {marginVertical: 15}]} onPress={() => this.navigation.navigate('BakingAll', {address: this.state.address})}><Text style={styles.button_text}>See All</Text></Button>
                                </View>
                                :
                                <View style={{height: 300}}></View>
                            }
                        </Content>
                    </ScrollView>
                </Container>
            )
        } else {
            return (
                <Container>
                    <Head navigation={this.navigation} route={this.route} />
                    <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                        <Spinner color='#319DFE' />
                    </Content>
                </Container>
            )
        }
    }
}

const styles = StyleSheet.create({
    active_menu: {
        minHeight: '100%',
        maxHeight: '100%'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: normalize(26),
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_info: {
        backgroundColor: "#121520",
        borderRadius: 20,
        padding: 20,
        marginBottom: 11
    },
    text: {
        color: '#737B9C',
        fontSize: normalize(18),
        fontFamily: 'Circe',
        lineHeight: 22
    },
    bold: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(18),
        color: '#319DFE'
    },
    h3: {
        color: '#EAECF2',
        fontSize: normalize(22),
        lineHeight: 26,
        fontFamily: 'Circe_bold',
        marginVertical: 10
    },
    input: {
        backgroundColor: '#090A11',
        borderRadius: 12,
        marginBottom: 15,
        paddingLeft: 24,
        height: 60,
        color: '#737B9C',
        fontFamily: 'Circe',
        fontSize: normalize(16),
    },
    button: {
        flexGrow: 1,
        backgroundColor: '#319DFE',
        height: 60,
        borderRadius: 12,
        flex: 1,
        justifyContent: 'center',
        marginBottom: 7,
        width: '100%',
    },
    button_text: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(16),
        color: 'white'
    },
    circ_bg: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 31
    },
    circ_block: {
        backgroundColor: '#1E2233',
        borderRadius: 10,
        flexDirection: 'row',
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    circ_text: {
        color: '#319DFE',
        fontFamily: 'Circe_bold',
        fontSize: normalize(65),
        marginBottom: -20
    },
    circ_sub: {
        fontFamily: 'Circe_bold',
        fontSize: 16,
        color: "#EAECF2",
        opacity: 0.4
    },
    circ_hash: {
        fontFamily: 'Circe_bold',
        fontSize: 32,
        color: 'rgba(234, 236, 242, 0.3)',
        marginRight: 5
    },
    circ_number: {
        fontFamily: 'Circe_bold',
        fontSize: 32,
        letterSpacing: 2,
        color: '#319DFE',
        textShadowColor: 'rgba(13, 106, 190, 0.8), 0px 4px 60px rgba(13, 106, 190, 0.7)',
        textShadowOffset: {width: 0, height: 1},
        textShadowRadius: 10
    },
    table: {
        backgroundColor: '#121520',
        borderRadius: 20,
        overflow: 'hidden'
    },
    thead: {
        backgroundColor: '#0D0F17'
    },
    tr: {
        flex: 1,
        flexDirection: 'row'
    },
    th: {
        color: '#737B9C',
        fontFamily: 'DINNextLTPro',
        paddingTop: 12,
        paddingBottom: 7,
        textAlign: 'center',
        fontSize: normalize(18)
    },
    td: {
        fontFamily: 'Circe',
        fontSize: normalize(16),
        paddingVertical: 12,
        textAlign: 'center',
        color: '#EAECF2',
        flex: 1,
        alignItems: 'center'
    },
    opacity0: {
        fontFamily: 'Circe',
        fontSize: normalize(16),
        color: '#EAECF2',
    },
    opacity5: {
        opacity: 0.5
    },
    red: {
        fontFamily: 'Circe',
        fontSize: normalize(16),
        color: '#F91525',
    },
    green: {
        fontFamily: 'Circe',
        fontSize: normalize(16),
        color: '#30DD36'
    }
});