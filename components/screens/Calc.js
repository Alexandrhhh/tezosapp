//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../Header';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import { normalize } from '../normalize';

export default class Calcscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assetValue: 0,
            loading: false,
            search: false,
            comparison: false,
            tezos: {},
            searchText: '',
            bakers: [],
            activeBakers: {},
            displayBakers: [],
            comparisonResult: [],
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    inputSearch(text) {
        this.setState({displayBakers: {}});
        this.setState({search: false});
        var arr = Array();
        if(text !== '') {
            this.state.bakers.map((baker, i) => (
                this.state.bakers[i].name.toLowerCase().indexOf(text.toLowerCase()) > -1 ?
                    arr.push(this.state.bakers[i])
                : ''
            ))
            this.setState({displayBakers: arr});
            this.setState({search: true});
        }
        this.setState({searchText: text});
    }

    comparison() {
        var arr = Array();
        Object.keys(this.state.activeBakers).map((item, i) => (
            this.state.activeBakers[item] ?
                arr.push(this.state.bakers[item - 1])
            :
            ''
        ))
        this.setState({comparisonResult: arr, comparison: true})

    }

    clear() {
        this.setState({activeBakers: {}, displayBakers: [], comparison: false,});
    }

    onChanged(text){
    let newText = '';
    let numbers = '0123456789';

    for (var i=0; i < text.length; i++) {
        if(numbers.indexOf(text[i]) > -1 ) {
            newText = newText + text[i];
        }
        else {
            // your call back function
            alert("please enter numbers only");
        }
    }
    this.setState({ assetValue: newText });
}

    componentDidMount() {
        fetch('https://api.tezos-nodes.com/v1/tezos')
        .then(response => response.json())
        .then(json => this.setState({ tezos: json }))

        fetch("https://api.tezos-nodes.com/v1/bakers")
        .then(response => response.json())
        .then(json => this.setState({ bakers: json }))

        this.loading = true;
    }
  render() {
    let {assetValue, tezos, bakers, activeBakers, searchText, displayBakers, comparisonResult} = this.state;
    if(this.loading) {
        return (
            <Container>
        <Head navigation={this.navigation} route={this.route} />
                <ScrollView style={{backgroundColor: '#1E2233'}}>
                    <Content style={{padding: 10, paddingTop: 0}}>
                        <View style={styles.header}>
                            <Text style={styles.h2}>Calculator rewards</Text>
                        </View>
                        <View style={styles.info_block}>
                            <View style={styles.input_bg}>
                                <Text style={[styles.din_text, {marginLeft: 7.5, marginBottom: -7.5}]}>AMOUNT DELEGATION</Text>
                                <View style={{margin:7.5}}>
                                    <TextInput style={styles.input} onChangeText={(text)=> this.onChanged(text)} value={`${this.state.assetValue}`} keyboardType='numeric' maxLength={5} keyboardAppearance={'dark'}></TextInput>
                                    <View style={styles.input_icon}>
                                        <Text style={styles.input_icon_text}>XTZ</Text>
                                    </View>
                                </View>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <View style={{flex: 1, flexGrow: 1, margin: 7.5, backgroundColor: '#1E2233', height: 77, borderRadius: 12, justifyContent: 'center', alignItems: 'center'}}>
                                        <Text style={styles.din_text}>TEZOS PRICE</Text>
                                        <Text style={styles.h4_strong}>$ {tezos.tezos_price_usd}</Text>
                                    </View>
                                    <View style={{flex: 1, flexGrow: 1, margin: 7.5, backgroundColor: '#1E2233', height: 77, borderRadius: 12, justifyContent: 'center', alignItems: 'center'}}>
                                        <Text style={styles.din_text}>ASSET VALUE</Text>
                                        <Text style={styles.h4_strong}>$ {(tezos.tezos_price_usd*assetValue).toFixed(2)}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.header}>
                            <Text style={styles.h2}>Selected for comparison</Text>
                        </View>
                        <TextInput style={styles.search} placeholderTextColor="#EAECF2" value={searchText} placeholder="Search" onChangeText={(text) => this.inputSearch(text)} onEndEditing={(e) => this.inputSearch(e.nativeEvent.text)}></TextInput>
                        <View style={[styles.info_block, {maxHeight: 364}]}>
                            <GestureHandlerScrollView>
                                {this.state.search ?
                                    displayBakers.length > 0 ?
                                        displayBakers.map((baker) => (
                                            <TouchableOpacity key={baker.rank} style={[styles.block, activeBakers[baker.rank] ? styles.block_active : '']} onPress={() => this.state.activeBakers[baker.rank] ? this.setState({activeBakers: {...this.state.activeBakers, [baker.rank]: false}}) : this.setState({activeBakers: {...this.state.activeBakers, [baker.rank]: true}})}>
                                                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                                        <View style={{width: 24, marginRight: 5}}><Image source={{uri: baker.logo_min}} style={{resizeMode:'contain',width:null,height:null, flex:1}}/></View>
                                                        <Text style={[styles.block_text, activeBakers[baker.rank] ? styles.block_text_active : '']}>{baker.name}</Text>
                                                    </View>
                                                    <Text style={[styles.block_text, {marginRight: 10}, activeBakers[baker.rank] ? styles.block_text_active : '']}>Yield: {baker.yield.toFixed(2)}%</Text>
                                                </View>
                                            </TouchableOpacity>
                                        ))
                                        :
                                        <Text style={{color: 'white', padding: 10}}>Baker not found</Text>
                                    :
                                    bakers.map((baker) => (
                                        <TouchableOpacity key={baker.rank} style={[styles.block, activeBakers[baker.rank] ? styles.block_active : '']} onPress={() => this.state.activeBakers[baker.rank] ? this.setState({activeBakers: {...this.state.activeBakers, [baker.rank]: false}}) : this.setState({activeBakers: {...this.state.activeBakers, [baker.rank]: true}})}>
                                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                                    <View style={{width: 24, marginRight: 5}}><Image source={{uri: baker.logo_min}} style={{resizeMode:'contain',width:null,height:null, flex:1}}/></View>
                                                    <Text style={[styles.block_text, activeBakers[baker.rank] ? styles.block_text_active : '']}>{baker.name}</Text>
                                                </View>
                                                <Text style={[styles.block_text, {marginRight: 10}, activeBakers[baker.rank] ? styles.block_text_active : '']}>Yield: {baker.yield.toFixed(2)}%</Text>
                                            </View>
                                        </TouchableOpacity>
                                    ))
                                }
                            </GestureHandlerScrollView>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <Button style={styles.calc_btn} onPress={() => this.comparison()}>
                                <Text style={styles.calc_btn_text} uppercase={false}>Calculate rewards</Text>
                            </Button>
                            <Button style={styles.clear_btn} onPress={() => this.clear()}>
                                <Text style={styles.clear_btn_text} uppercase={false}>Clear list</Text>
                            </Button>
                        </View>
                        {
                            this.state.comparison ?
                            <View>
                                <View style={styles.header}>
                                    <Text style={styles.h2}>Сomparison rewards</Text>
                                </View>
                                <View>
                                    <ScrollView>
                                        <View style={{flex: 1, flexDirection: 'row', flexWrap: 'nowrap', borderRadius: 20, overflow: 'hidden'}}>
                                            <View style={{maxWidth: '55%', overflow: 'hidden'}}>
                                                <View style={styles.theadtr}>
                                                    <Text style={[styles.th, {width: '100%'}]}># Baker</Text>
                                                </View>
                                                {comparisonResult.map((baker) => (
                                                    <TouchableOpacity activeOpacity={.8} key={baker.rank} onPress={() => this.navigation.navigate('Baker', {tz: baker.address})}>
                                                        <View style={styles.tbodytr}>
                                                            <View style={[styles.td, {width: '100%', flex: 1, flexDirection: 'row', alignItems: 'center'}]}>
                                                                <View style={{width: 24, marginRight: 5}}><Image source={{uri: baker.logo_min}} style={{resizeMode:'contain',width:null,height:null, flex:1}}/></View>
                                                                <Text style={styles.table_text}>{baker.name}</Text>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                ))}
                                            </View>
                                            <View style={{maxWidth: '45%', borderLeftWidth: 1, borderLeftColor: '#30333F'}}>
                                                <ScrollView horizontal={true}>
                                                    <View>
                                                        <View style={styles.theadtr}>
                                                            <Text style={[styles.th, {width: 150, textAlign: 'center'}]}>Cycle reward</Text>
                                                            <Text style={[styles.th, {width: 150, textAlign: 'center'}]}>Monthly reward</Text>
                                                            <Text style={[styles.th, {width: 150, textAlign: 'center'}]}>Yearly reward</Text>
                                                        </View>
                                                        {comparisonResult.map((baker) => (
                                                            <View style={styles.tbodytr} key={baker.rank}>
                                                                <View style={[styles.td, {width: 150, flexDirection: 'column', paddingLeft: 12, paddingRight: 12}]}>
                                                                    <View style={styles.xtz}><Text style={styles.calc_text}>{(((this.state.assetValue * baker.yield)/100)/128.3).toFixed(2)} XTZ</Text></View>
                                                                    <View style={styles.dollar}><Text style={styles.calc_text}>{(((this.state.assetValue * baker.yield * tezos.tezos_price_usd)/100)/128.3).toFixed(2)} $</Text></View>
                                                                </View>
                                                                <View style={[styles.td, {width: 150, flexDirection: 'column', paddingLeft: 12, paddingRight: 12}]}>
                                                                    <View style={styles.xtz}><Text style={styles.calc_text}>{((((this.state.assetValue * baker.yield)/100)/128.3)*10.69).toFixed(2)} XTZ</Text></View>
                                                                    <View style={styles.dollar}><Text style={styles.calc_text}>{((((this.state.assetValue * baker.yield * tezos.tezos_price_usd)/100)/128.3)*10.69).toFixed(2)} $</Text></View>
                                                                </View>
                                                                <View style={[styles.td, {width: 150, flexDirection: 'column', paddingLeft: 12, paddingRight: 12}]}>
                                                                    <View style={styles.xtz}><Text style={styles.calc_text}>{((this.state.assetValue * baker.yield)/100).toFixed(2)} XTZ</Text></View>
                                                                    <View style={styles.dollar}><Text style={styles.calc_text}>{((this.state.assetValue * baker.yield * tezos.tezos_price_usd)/100).toFixed(2)} $</Text></View>
                                                                </View>
                                                            </View>
                                                        ))}
                                                    </View>
                                                </ScrollView>
                                            </View>
                                        </View>
                                    </ScrollView>
                                </View>
                            </View>
                            :
                            <View></View>
                        }
                    </Content>
                </ScrollView>
            </Container>
        )
    } else {
        return (
                   <Container>
                      <Head navigation={this.navigation} route={this.route} />
                       <Content style={{backgroundColor: '#1E2233', color: '#EAECF2'}}>
                           <Spinner color='#319DFE' />
                       </Content>
                   </Container>
        )
    }
  }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: normalize(26),
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    info_block: {
        backgroundColor: "#121520",
        borderRadius: 20,
        overflow: 'hidden',
        padding: 7.5
    },
    din_text: {
        fontFamily: 'DINNextLTPro',
        fontSize: normalize(10),
        color: '#737B9C',
    },
    h4_strong: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(22),
        color: '#EAECF2'
    },
    input_bg: {
        padding: 15.5
    },
    input: {
        fontFamily: 'Circe',
        height: 55,
        borderRadius: 12,
        backgroundColor: '#090A11',
        marginTop: 3,
        paddingLeft: 20,
        fontSize: normalize(22),
        color: '#EAECF2'
    },
    input_icon: {
        position: 'absolute',
        height: 47,
        backgroundColor: '#1E2233',
        top: 7,
        right: 4,
        borderRadius: 9,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    input_icon_text: {
        color: '#737B9C',
        fontWeight: '700'
    },
    block_text: {
        fontFamily: 'Circe',
        fontSize: normalize(14),
        color: '#EAECF2',
        lineHeight: 56
    },
    block_text_active: {
        color: '#000000'
    },
    block: {
        margin:7.5,
        backgroundColor: "#1E2233",
        borderRadius: 12,
        height: 54,
        paddingLeft: 10,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    block_active: {
        backgroundColor: "#319DFE",
    },
    search: {
        backgroundColor: '#090A11',
        borderRadius: 100,
        padding: 10,
        paddingLeft: 23,
        fontFamily: 'Circe',
        fontSize: normalize(16),
        color: '#EAECF2',
        marginBottom: 12
    },
    calc_btn: {
        borderRadius: 12,
        backgroundColor: '#319DFE',
        height: 60,
        margin: 7.5,
        marginTop: 15,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 0
    },
    calc_btn_text: {
        fontFamily: "Circe_bold",
        fontSize: normalize(16),
        color: '#FFFFFF',
    },
    clear_btn: {
        borderWidth: 1,
        borderColor: '#333952',
        height: 60,
        borderRadius: 12,
        margin: 7.5,
        marginTop: 15,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        elevation: 0
    },
    clear_btn_text: {
        fontFamily: "Circe_bold",
        fontSize: normalize(16),
        color: '#6F7693',
    },
    tbodytr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 61,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#30333F',
        backgroundColor: '#121520'
    },
    theadtr: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 61,
        justifyContent: 'center',
        flexWrap: 'nowrap'
    },
    th: {
        backgroundColor: '#0D0F17',
        paddingTop: 23,
        paddingBottom: 23,
        paddingLeft: 12,
        paddingRight: 12,
        color: '#737B9C',
        fontFamily: 'DINNextLTPro',
        ...Platform.select({
            ios: {
                paddingTop: 24,
            },
        }),
    },
    td: {
        paddingLeft: 15,
        color: '#EAECF2',
        fontFamily: 'Circe',
        fontSize: normalize(16),
        flex: 1,
        flexDirection: 'row',
        height: 51,
        alignItems: 'center',
        justifyContent: 'center'
    },
    table_text: {
        color: '#EAECF2',
        fontFamily: 'Circe',
        fontSize: normalize(16),
        flex: 1,
        flexWrap: 'wrap'
    },
    xtz: {
        backgroundColor: '#306EA6',
        textAlign: 'center',
        borderRadius: 3,
        marginTop: 2,
        marginBottom: 2,
        flex: 1,
        justifyContent: 'center',
        flexGrow: 1,
        width: '100%'
    },
    dollar: {
        backgroundColor: '#1E2233',
        textAlign: 'center',
        borderRadius: 3,
        marginTop: 2,
        marginBottom: 2,
        flex: 1,
        flexGrow: 1,
        width: '100%'
    },
    calc_text: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(12),
        color: '#ffffff',
        textAlign: 'center',
        lineHeight: 25
    }
})
