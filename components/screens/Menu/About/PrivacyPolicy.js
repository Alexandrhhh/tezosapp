//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../../../Header';
import { normalize } from '../../../normalize';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';

export default class PrivacyPolicyscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

  render() {
      return (
          <Container>
    <Head sub={true} navigation={this.navigation} route={this.route}/>
              <ScrollView style={{backgroundColor: '#1E2233'}}>
                  <Content style={{padding:10, paddingTop: 0}}>
                      <View style={styles.header}>
                          <Text style={styles.h2}>Privacy policy</Text>
                      </View>
                      <View style={styles.block_info}>
                          <Text style={styles.text}>Built the Tezos Nodes app as an Open Source app. This SERVICE is provided by at no cost and is intended for use as is.{"\n"}
                          This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.{"\n"}
                          If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.{"\n"}
                          The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Tezos Nodes unless otherwise defined in this Privacy Policy.</Text>
                            <Text style={styles.h3}>Information Collection and Use</Text>
                          <Text style={styles.text}>For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information, including but not limited to e-mail. The information that I request will be retained on your device and is not collected by me in any way.{"\n"}
                          The app does use third party services that may collect information used to identify you.</Text>
                            <Text style={styles.h3}>Log Data</Text>
                          <Text style={styles.text}>I want to inform you that whenever you use my Service, in a case of an error in the app I collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics.</Text>
                          <Text style={styles.h3}>Cookies</Text>
                          <Text style={styles.text}>
                              Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.{"\n"}
                              This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.
                          </Text>
                          <Text style={styles.h3}>Service Providers</Text>
                          <Text style={styles.text}>
                              I may employ third-party companies and individuals due to the following reasons:{"\n"}
                                <Text style={styles.bold}>•</Text> To facilitate our Service;{"\n"}
                                <Text style={styles.bold}>•</Text> To provide the Service on our behalf;{"\n"}
                                <Text style={styles.bold}>•</Text> To perform Service-related services;{"\n"}
                                <Text style={styles.bold}>•</Text> To assist us in analyzing how our Service is used.{"\n"}
                              I want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.
                          </Text>
                          <Text style={styles.h3}>Security</Text>

                          <Text style={[styles.text, {marginBottom: 0}]}>If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact us at support@tezos-nodes.com.</Text>
                      </View>
                  </Content>
              </ScrollView>
          </Container>
      );
  }
}

const styles = StyleSheet.create({
    active_menu: {
        minHeight: '100%',
        maxHeight: '100%'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: normalize(26),
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_info: {
        backgroundColor: "#121520",
        borderRadius: 20,
        padding: 20
    },
    text: {
        color: '#737B9C',
        fontSize: normalize(18),
        fontFamily: 'Circe',
        marginBottom: 18,
        lineHeight: 22
    },
    bold: {
        fontFamily: 'Circe_bold',
        fontSize: normalize(18),
        color: '#319DFE'
    },
    h3: {
        color: '#EAECF2',
        fontSize: normalize(22),
        lineHeight: 26,
        fontFamily: 'Circe_bold',
        marginVertical: 10
    },
});