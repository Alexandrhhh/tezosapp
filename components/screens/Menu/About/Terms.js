//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../../../Header';
import { normalize } from '../../../normalize';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';

export default class Termsscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

  render() {
      return (
          <Container>
    <Head sub={true} navigation={this.navigation} route={this.route}/>
              <ScrollView style={{backgroundColor: '#1E2233'}}>
                  <Content style={{padding:10, paddingTop: 0}}>
                      <View style={styles.header}>
                          <Text style={styles.h2}>Terms</Text>
                      </View>
                      <View style={styles.block_info}>
                          <Text style={[styles.h3, {marginTop:0}]}>Main</Text>
                          <Text style={styles.text}>
                              Tezos-nodes.com is a public information website displaying publicly available Tezos blockchain data and aggregate statistics. Tezos-nodes.com is developed and operated by Tezos Nodes team and provided to you free of charge.{"\n"}
                              Data is subject to the MIT License. All information is displayed as is and we provide no guarantee for correctness or fitness for a particular purpose. Information does not constitute investment advice, financial advice, trading advice, or any other sort of advice.{"\n"}
                              We disclaim any responsibility for any missing or wrong information. You understand that you are using any and all information available here at your own risk.
                          </Text>
                          <Text style={styles.h3}>Terms</Text>
                          <Text style={styles.text}>This page contains the terms that apply to your use of the Website (the “Terms”).  The Site is owned and operated by Tezos Nodes team (“Tezos Nodes,” “we,” or “us”). Tezos Nodes offers a structured set of data, text, and media files that includes public data from the Tezos blockchain, cryptocurrency markets and other data (the “Data”). The Data is provided through the Site, mobile applications, API, data exports and widgets (collectively, the “Platform,” and together with the Data, the “Services”).</Text>
                          <Text style={styles.text}>These Terms of Service govern your use of the Site and the Services, and may be updated by us from time to time without notice to you. Please read these Terms of Service carefully before using the Site and the Services. By accessing or using the Site or the Services, you acknowledge that you have read, understood, and agree to be legally bound by the terms and conditions of these Terms of Service and the terms and conditions of our Privacy Policy, which is hereby incorporated by reference (collectively with these Terms of Service, this “Agreement”). If you do not agree to any of these terms, then please do not access or use the Site or the Services.</Text>
                          <Text style={styles.h3}>Access to the Services</Text>
                          <Text style={styles.text}>Subject to the terms and conditions of these Terms of Service, we may provide access to the Services in a number of ways, including, but not be limited to, the offering of any materials displayed or performed on the Site and related features (including, but not limited to text, data, software, graphics, photographs, images, illustrations, audio clips and video clips), copying Content directly from the Site, through export data files or other data dumps directly from tezos-nodes.com, or through the API interface Tezos Nodes. We may change, suspend or discontinue the Services for any reason, at any time, including the availability of any feature or Content. We may also restrict your access to all of the Services without notice or liability.</Text>
                          <Text style={styles.h3}>Communications</Text>
                          <Text style={styles.text}>With respect to all emails and communications you send to us, including, but not limited to, feedback, questions, comments, suggestions, and the like, we shall be free to use any ideas, concepts, know-how, or techniques contained in your communications for any purpose whatsoever, including but not limited to, the development, production, and marketing of products and services that incorporate such information without compensation or attribution to you.</Text>
                          <Text style={styles.h3}>Third Party Sites</Text>
                          <Text style={styles.text}>Periodically, links may be established from the Site to external websites or content (including, but not limited to, photographs, graphics, pictures, text, articles, designs, video, music, sound, information, applications, and software) operated by third parties (each, a “Third Party Site“). These links to Third Party Sites are provided for your convenience only and do not imply that we endorse any Third Party Site or any content therein. We do not control and we are not responsible or liable for any Third Party Site or any content, advertising, products, or other materials on or available from any Third Party Site. Access to a Third Party Site is at your own risk, and we will have no liability for any damages or loss caused or alleged to be caused by or in connection with any purchase, use of or reliance on any content, goods, or services available on or through any Third Party Site.</Text>
                          <Text style={styles.h3}>Personal Information</Text>
                          <Text style={styles.text}>Any personal information that you provide to us through your use of the Site is subject to the terms of our Privacy Policy, which is hereby incorporated by reference in its entirety.</Text>
                          <Text style={styles.h3}>User Accounts</Text>
                          <Text style={styles.text}>You may create a user account (“Account”) and provide certain personal information in order to use some of the features that are offered through the Services. Providing with your personal information is your choice. You acknowledge and agree that you have no ownership or other proprietary interest in such Account.</Text>
                          <Text style={styles.text}>Your Account is for personal, non-commercial use only. To create an Account, you must be have a valid email address. You may not impersonate someone else, create or use an Account for anyone other than yourself, provide an email address or other personal information other than your own or create multiple Accounts. Please read our Privacy Policy for more information.</Text>
                          <Text style={styles.h3}>Disclaimers</Text>
                          <Text style={styles.text}>No Investment Advice</Text>
                          <Text style={styles.text}>The information provided on this website does not constitute investment advice, financial advice, trading advice, or any other sort of advice and you should not treat any of the website's content as such. We does not recommend that any cryptocurrency should be bought, sold, or held by you.</Text>
                          <Text style={styles.h3}>Limitation of Liability</Text>
                          <Text style={styles.text}>You acknowledge that Tezos-nodes.com Does not represent or warrant that the data, content, or any other information provided through the website or services will be accurate or complete. </Text>
                          <Text style={styles.h3}>Revisions to these Terms of Service</Text>
                          <Text style={styles.text}>We may revise these Terms of Service at any time and from time to time and will post the revised Terms of Service on the Site. Your continued use of the Site after any revisions to these Terms of Service are posted will be considered acceptance by you of those changes. We encourage you to visit this page from time to time to review the then current Terms of Service.</Text>
                          <Text style={styles.h3}>Miscellaneous</Text>
                          <Text style={[styles.text, {marginBottom: 0}]}>The Terms contain the entire agreement between you and Tezos Nodes regarding the use of the Services, and supersede any prior agreement between you and the Company on such subject matter. The parties acknowledge that no reliance is placed on any representation made but not expressly contained in these Terms.</Text>
                      </View>
                  </Content>
              </ScrollView>
          </Container>
      );
  }
}

const styles = StyleSheet.create({
    active_menu: {
        minHeight: '100%',
        maxHeight: '100%'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 10
    },
    h2: {
        color: '#EAECF2',
        fontSize: normalize(26),
        fontFamily: 'Circe_bold',
        marginRight: 11
    },
    block_info: {
        backgroundColor: "#121520",
        borderRadius: 20,
        padding: 20
    },
    text: {
        color: '#737B9C',
        fontSize: normalize(18),
        fontFamily: 'Circe',
        marginBottom: 18,
        lineHeight: 22
    },
    h3: {
        color: '#EAECF2',
        fontSize: normalize(22),
        lineHeight: 26,
        fontFamily: 'Circe_bold',
        marginVertical: 10
    },
})