//Homescreen.js

import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView, TextInput, StatusBar, TouchableOpacity, Linking} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Header,Left, Body, Right, Title, Content, Footer, FooterTab, Button, Text, Spinner} from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Head from '../../Header';
import { normalize } from '../../normalize';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';
import {SvgXml} from 'react-native-svg';
import * as WebBrowser from 'expo-web-browser';

export default class Supportscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

    async openUrl(uri) {
        await WebBrowser.openBrowserAsync(uri);
    }

  render() {
      const svgUrl = '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 23C18.0751 23 23 18.0751 23 12C23 5.92487 18.0751 1 12 1C5.92487 1 1 5.92487 1 12C1 18.0751 5.92487 23 12 23Z" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M12 1V23" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M17.4955 12.0656C17.4955 18.1799 15.0386 23 12.0002 23C8.96183 23 6.50488 18.1799 6.50488 12.0656C6.50488 5.95141 8.96183 1 12.0002 1C15.0386 1 17.4955 5.95141 17.4955 12.0656Z" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M2.34082 6.58793L21.6588 6.44727" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M2.34082 17.4489L21.6588 17.2988" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/><path d="M1 12H23" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10"/></svg>';
      const svgTwitter = '<svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22 3.04897L20.0147 2.78746L20.533 0.853363L18.2073 1.47658C17.4639 1.03615 16.6198 0.804688 15.7477 0.804688C13.0823 0.804688 10.9137 2.97327 10.9137 5.63867V6.26575C7.58701 6.08481 4.52464 4.50655 2.43361 1.87286L1.89297 1.19191L1.39867 1.9071C1.20296 2.19009 1.03696 2.49524 0.905029 2.81364C0.49498 3.8036 0.422302 4.87312 0.695053 5.90689C0.855347 6.515 1.13548 7.0882 1.51079 7.58905L0.744568 7.77301L0.894958 8.39975C1.2946 10.0638 2.51686 11.3582 4.08606 11.8765L3.3326 12.7587L3.82271 13.1773C4.56644 13.8125 5.48271 14.2059 6.44666 14.3116C5.12453 15.2821 3.51857 15.8177 1.84732 15.8177C1.47806 15.8177 1.1041 15.791 0.735672 15.7383L0 15.6331V16.7186L0.276779 16.9109C2.44788 18.4194 4.9968 19.2168 7.6481 19.2168C11.1029 19.2168 14.3507 17.8715 16.7936 15.4287C19.2364 12.9858 20.5817 9.73799 20.5817 6.2832V5.63867C20.5817 5.41124 20.5658 5.18381 20.5342 4.95923L22 3.04897ZM19.22 4.92029C19.2681 5.15494 19.2926 5.39664 19.2926 5.63867V6.2832C19.2926 12.704 14.0689 17.9277 7.6481 17.9277C6.07672 17.9277 4.54512 17.6167 3.12447 17.0167C5.29472 16.7085 7.29277 15.6214 8.73944 13.93L9.127 13.4768L8.41986 12.77L8.04741 12.887C7.6998 12.9961 7.33842 13.0513 6.97319 13.0513C6.34343 13.0513 5.72777 12.8836 5.18814 12.5711L6.59705 10.9216L5.33585 10.8224C4.00969 10.7182 2.8779 9.89644 2.35522 8.71194L4.17921 8.2742L3.15518 7.45158C1.9581 6.48965 1.51431 4.84291 2.05461 3.41135C4.50148 6.06735 7.9138 7.57227 11.5583 7.57227H12.2028V5.63867C12.2028 3.68393 13.7931 2.09375 15.7477 2.09375C16.4621 2.09375 17.1504 2.30524 17.7385 2.70522L17.9828 2.87122L18.71 2.67635L18.3891 3.87376L19.6181 4.03557L19.1607 4.63176L19.22 4.92029Z" fill="#319DFE"/></svg>';
      const svgTelegram = '<svg width="23" height="21" viewBox="0 0 23 21" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.2807 1.11914L0.720703 9.54914L6.1507 12.3591L17.6607 5.73914L9.1007 13.2591V19.8791L11.8907 16.2591L18.5707 19.8791L22.2807 1.11914Z" stroke="#319DFE" stroke-width="1.1" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/></svg>';
      const svgMail = '<svg width="22" height="17" viewBox="0 0 22 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.0664 0H1.93359C0.868742 0 0 0.867797 0 1.93359V14.8242C0 15.8864 0.864875 16.7578 1.93359 16.7578H20.0664C21.1286 16.7578 22 15.8929 22 14.8242V1.93359C22 0.871406 21.1351 0 20.0664 0ZM19.7994 1.28906L11.041 10.0475L2.20683 1.28906H19.7994ZM1.28906 14.5573V2.19437L7.49714 8.34922L1.28906 14.5573ZM2.20056 15.4688L8.41255 9.25676L10.5892 11.4147C10.8412 11.6646 11.2478 11.6637 11.4987 11.4128L13.6211 9.2904L19.7994 15.4688H2.20056ZM20.7109 14.5573L14.5326 8.37891L20.7109 2.20052V14.5573Z" fill="#319DFE"/></svg>';
      const svgDiscord = '<svg width="26" height="22" viewBox="0 0 26 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.0386 13.1951C25.0338 12.918 24.8071 12.6955 24.5299 12.6955C24.2453 12.6956 24.0165 12.9296 24.0219 13.2143C24.052 14.8123 23.9926 16.3254 23.8488 17.605C21.5716 19.3708 18.3819 20.417 16.1675 20.6955L15.0433 17.6852C16.601 17.4413 18.9732 16.7585 20.8976 14.863C21.0629 14.7002 21.0961 14.4459 20.9781 14.2461C20.8603 14.0464 20.6215 13.9528 20.3991 14.0187C20.1718 14.0863 19.8692 14.2045 19.5488 14.3297C19.187 14.471 18.777 14.6312 18.5461 14.686C16.7067 15.1235 14.8423 15.3451 13.0039 15.3451C13.0027 15.3451 12.9997 15.3451 12.9997 15.3451H12.9957C11.1575 15.3451 9.29283 15.1234 7.45339 14.686C7.22264 14.6312 6.81258 14.471 6.45083 14.3297C6.14258 14.2093 5.85077 14.0953 5.6267 14.0266C5.41827 13.9628 5.18495 14.0239 5.05377 14.198C4.89895 14.4034 4.92314 14.6868 5.10202 14.8631C7.03327 16.7652 9.40158 17.4498 10.9561 17.694L9.83264 20.6954C7.61833 20.4169 4.42827 19.3707 2.15108 17.6049C1.67502 13.3703 2.1222 6.57789 3.6867 2.61339C5.12058 1.96214 7.22708 1.77695 9.20545 1.69814L6.05045 3.16114C5.81089 3.27214 5.69727 3.54902 5.78964 3.79645C5.88214 4.04383 6.14945 4.17827 6.40314 4.10495C8.59277 3.47245 10.8122 3.15164 12.9998 3.15164C15.1875 3.15164 17.407 3.47245 19.5965 4.10495C19.85 4.17795 20.1175 4.04377 20.21 3.79645C20.3025 3.54908 20.1889 3.27227 19.9492 3.1612L16.7942 1.6982C18.7726 1.77702 20.8791 1.9622 22.313 2.61345C23.1686 4.78183 23.69 7.79602 23.909 10.7751C23.9285 11.0403 24.1499 11.245 24.4158 11.245H24.4176C24.7135 11.245 24.9468 10.9935 24.9252 10.6984C24.694 7.5427 24.1242 4.35183 23.1751 2.04952C23.1286 1.93683 23.043 1.84464 22.9338 1.79014C20.9251 0.785953 17.7815 0.705203 15.2555 0.640203C14.9463 0.632266 14.6476 0.624641 14.3632 0.615453C14.1235 0.608453 13.9109 0.770078 13.8554 1.00377C13.7999 1.23739 13.9166 1.47783 14.1345 1.57883L15.6724 2.29202C14.7793 2.19158 13.8871 2.14102 12.9997 2.14102C12.1123 2.14102 11.22 2.19152 10.327 2.29202L11.8649 1.57883C12.0828 1.47783 12.1995 1.23739 12.144 1.00377C12.0885 0.770078 11.8765 0.608703 11.6361 0.615453C11.3518 0.624641 11.0531 0.632203 10.7439 0.640203C8.21789 0.705141 5.0742 0.785891 3.06552 1.79014C2.95645 1.84464 2.87077 1.93683 2.82427 2.04952C1.09045 6.25589 0.622579 13.4285 1.17195 17.9327C1.18783 18.0633 1.25402 18.1824 1.35639 18.2649C3.9517 20.3571 7.62952 21.5194 10.123 21.7432C10.1383 21.7446 10.1534 21.7452 10.1685 21.7452C10.3775 21.7452 10.5671 21.6157 10.6414 21.4171L12.1256 17.4518C12.1984 17.2575 12.1461 17.033 11.9802 16.8897C11.899 16.8196 11.7951 16.7815 11.6884 16.7705C10.9095 16.691 9.73052 16.4816 8.48839 15.9374C9.98995 16.2153 11.5007 16.3558 12.9958 16.3558H12.9998C12.9998 16.3558 12.9999 16.3558 12.9999 16.3558H13.004C14.4922 16.3558 15.9963 16.2166 17.491 15.9411C16.2584 16.4753 15.0878 16.6823 14.312 16.7616C14.2053 16.7725 14.1014 16.8105 14.02 16.8804C13.8537 17.0233 13.8012 17.248 13.8739 17.4426L15.3581 21.4168C15.4323 21.6156 15.622 21.7453 15.8311 21.7453C15.8462 21.7453 15.8614 21.7446 15.8766 21.7433C18.3701 21.5195 22.048 20.3571 24.6432 18.265C24.7456 18.1825 24.8117 18.0633 24.8276 17.9328C24.9962 16.5506 25.0689 14.917 25.0386 13.1951Z" fill="#319DFE"/><path d="M8.28398 6.45312C6.51955 6.45312 5.08398 7.88869 5.08398 9.65312C5.08398 11.4176 6.51955 12.8531 8.28398 12.8531C10.0484 12.8531 11.484 11.4176 11.484 9.65312C11.484 7.88869 10.0484 6.45312 8.28398 6.45312ZM8.28398 11.8426C7.07673 11.8426 6.09448 10.8604 6.09448 9.65312C6.09448 8.44581 7.07673 7.46362 8.28398 7.46362C9.49123 7.46362 10.4735 8.44581 10.4735 9.65312C10.4735 10.8604 9.49123 11.8426 8.28398 11.8426Z" fill="#319DFE"/><path d="M17.7161 6.45312C15.9517 6.45312 14.5161 7.88869 14.5161 9.65312C14.5161 11.4176 15.9517 12.8531 17.7161 12.8531C19.4806 12.8531 20.9161 11.4176 20.9161 9.65312C20.9161 7.88869 19.4806 6.45312 17.7161 6.45312ZM17.7161 11.8426C16.5089 11.8426 15.5266 10.8604 15.5266 9.65312C15.5266 8.44581 16.5089 7.46362 17.7161 7.46362C18.9234 7.46362 19.9056 8.44581 19.9056 9.65312C19.9056 10.8604 18.9233 11.8426 17.7161 11.8426Z" fill="#319DFE"/></svg>';
      return (
          <Container>
    <Head sub={true} navigation={this.navigation} route={this.route}/>
              <ScrollView style={{backgroundColor: '#1E2233'}}>
                  <Content style={{padding:10, paddingTop: 0}}>
                      <View style={styles.header}>
                          <Text style={styles.h2}>Support</Text>
                      </View>
                      <View style={styles.block_info}>
                          <TouchableOpacity style={styles.link} onPress={() => this.openUrl('https://tezos-nodes.com')}>
                              <SvgXml xml={svgUrl} style={styles.icon} />
                              <Text style={styles.text}>www.tezos-nodes.com</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.link} onPress={() => this.openUrl('https://twitter.com/tezosnodes')}>
                              <SvgXml xml={svgTwitter} style={styles.icon} />
                              <Text style={styles.text}>@tezosnodes</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.link} onPress={() => this.openUrl('https://t.me/tezosnodes')}>
                              <SvgXml xml={svgTelegram} style={styles.icon} />
                              <Text style={styles.text}>@tezosnodes</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.link} onPress={() => Linking.openURL('mailto:support@tezos-nodes.com')}>
                              <SvgXml xml={svgMail} style={styles.icon} />
                              <Text style={styles.text}>support@tezos-nodes.com</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.link} onPress={() => this.openUrl('https://discord.com/invite/udZwhQn')}>
                              <SvgXml xml={svgDiscord} style={[styles.icon, {marginRight: 17}]} />
                              <Text style={styles.text}>Tezos Discord Server</Text>
                          </TouchableOpacity>
                      </View>
                  </Content>
              </ScrollView>
          </Container>
      );
  }
}

const styles = StyleSheet.create({
        active_menu: {
            minHeight: '100%',
            maxHeight: '100%'
        },
        text: {
            fontSize: normalize(16),
            fontFamily: 'Circe',
            color: '#EAECF2'
        },
        header: {
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 30,
            marginBottom: 10
        },
        h2: {
            color: '#EAECF2',
            fontSize: normalize(26),
            fontFamily: 'Circe_bold',
            marginRight: 11
        },
        block_info: {
            backgroundColor: "#121520",
            borderRadius: 20,
            padding: 11
        },
        link: {
            flex: 1,
            paddingHorizontal: 20,
            height: 54,
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#1E2233',
            borderRadius: 12,
            margin: 5
        },
        icon: {
            marginRight: 20
        }
    }
)