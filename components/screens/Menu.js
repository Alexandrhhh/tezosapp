//Homescreen.js

import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, AsyncStorage} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Container, Content, Text, Spinner} from 'native-base';
import Head from '../Header';
import {SvgXml} from 'react-native-svg';
import { normalize } from '../normalize';

export default class Menuscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.navigation = props.navigation;
        this.route = props.route;
    }

  render() {
      const svgArrow = '<svg width="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.78279 5.99274L1.72295 0.207681C1.5828 0.0737718 1.3957 0 1.1962 0C0.996706 0 0.809609 0.0737718 0.669453 0.207681L0.223188 0.633613C-0.0671998 0.911155 -0.0671998 1.36224 0.223188 1.63936L5.31178 6.4973L0.217542 11.3606C0.0773851 11.4945 0 11.6731 0 11.8634C0 12.054 0.0773851 12.2325 0.217542 12.3665L0.663807 12.7923C0.804074 12.9262 0.99106 13 1.19056 13C1.39005 13 1.57715 12.9262 1.71731 12.7923L7.78279 7.00198C7.92328 6.86764 8.00044 6.68829 8 6.49762C8.00044 6.30622 7.92328 6.12697 7.78279 5.99274Z" fill="#319DFE"/></svg>';
      return (
          <Container>
              <Head navigation={this.navigation} route={this.route}/>
              <Content style={{backgroundColor: '#121520'}}>
              <View style={[{width: '100%', backgroundColor: '#121520', padding: 10, paddingTop: 30}, styles.active_menu]}>
                  <View style={{borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#2C3140', padding: 10, marginTop: -1}}>
                      <TouchableOpacity style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', maxHeight: 59}} onPress={() => this.navigation.navigate('About')} >
                          <Text style={styles.text}>About</Text>
                          <SvgXml xml={svgArrow} />
                      </TouchableOpacity>
                  </View>
                  <View style={{borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#2C3140', padding: 10, marginTop: -1}}>
                      <TouchableOpacity style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', maxHeight: 59}} onPress={() => this.navigation.navigate('Support')}>
                          <Text style={styles.text}>Support</Text>
                          <SvgXml xml={svgArrow} />
                      </TouchableOpacity>
                  </View>
              </View>
              </Content>
          </Container>
      );
  }
}

const styles = StyleSheet.create({
    active_menu: {
        minHeight: '100%',
        maxHeight: '100%'
    },
    text: {
        fontSize: normalize(22),
        fontFamily: 'Circe',
        color: '#EAECF2'
    }
})