import { Platform } from 'react-native';

export function normalize(size) {
  if (Platform.OS === 'ios') {
    return Math.round(size)
  } else {
      if(size < 10) {
          return Math.round(size) - 2
      } else {
           return Math.round(size) - 3
      }
  }
}