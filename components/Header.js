import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, AsyncStorage } from 'react-native';
import { Button, Text, Spinner } from 'native-base';
import {SvgXml} from 'react-native-svg';
import { normalize } from './normalize';

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.navigation = props.navigation
        this.route = props.route;
        this.state = {
            loading: false,
            reading: [],
            news: [],
            badge: 0,
            loading_badge: false
        }
        if(props.sub) {
            this.sub = props.sub;
        }
    }

    async updateBadge() {
        await AsyncStorage.getItem('reading')
        .then(reading => {
            if(reading !== null) {
                this.setState({reading: JSON.parse(reading)})
            }
        })
        await AsyncStorage.getItem('news')
        .then(news => {
            if(news !== null) {
                this.setState({news: JSON.parse(news)})
            }
        })
        let badge = this.state.news.length;
        if(this.state.news.length > 0) {
            this.state.news.map((i) => {
                if(this.state.reading.length > 0) {
                    if(this.state.reading.indexOf(i) != -1) {
                        badge--;
                    }
                }
            })
        }
        this.setState({badge: badge})
    }

    componentWillUnmount() {
        this._update();
    }

    componentDidMount() {
        fetch("https://api.tezos-nodes.com/v1/news")
        .then(response => response.json())
        .then(json => {
            let arr = Array();
            json.map((news) => {
                arr.push(news.id)
            })
            AsyncStorage.setItem('news', JSON.stringify(arr));
            this.updateBadge();
            this.setState({loading_badge: true})
        })
        this.setState({loading: true})
        this._update = this.navigation.addListener('focus', () => {
          this.updateBadge();
        });
        this.updateBadge();
    }

    render() {
        const svgArrow = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.78279 5.99274L1.72295 0.207681C1.5828 0.0737718 1.3957 0 1.1962 0C0.996706 0 0.809609 0.0737718 0.669453 0.207681L0.223188 0.633613C-0.0671998 0.911155 -0.0671998 1.36224 0.223188 1.63936L5.31178 6.4973L0.217542 11.3606C0.0773851 11.4945 0 11.6731 0 11.8634C0 12.054 0.0773851 12.2325 0.217542 12.3665L0.663807 12.7923C0.804074 12.9262 0.99106 13 1.19056 13C1.39005 13 1.57715 12.9262 1.71731 12.7923L7.78279 7.00198C7.92328 6.86764 8.00044 6.68829 8 6.49762C8.00044 6.30622 7.92328 6.12697 7.78279 5.99274Z" fill="#319DFE"/></svg>';
        const svgHome = '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.6574 8.24873L19.558 8.36108C19.6881 8.4761 19.7798 8.62817 19.8208 8.79684C19.8618 8.96551 19.8502 9.1427 19.7875 9.30458L19.7875 9.30474C19.7258 9.46448 19.6174 9.60189 19.4763 9.69895C19.3353 9.79602 19.1682 9.84823 18.997 9.84873H17.9974H17.8474V9.99873V18.9987C17.8474 19.2242 17.7579 19.4404 17.5985 19.5998C17.4391 19.7592 17.2229 19.8487 16.9974 19.8487H2.99743C2.772 19.8487 2.5558 19.7592 2.39639 19.5998C2.23699 19.4404 2.14743 19.2242 2.14743 18.9987V9.99873V9.84873H1.99743H0.997877C0.826652 9.84823 0.659571 9.79602 0.518518 9.69895C0.377466 9.60189 0.269017 9.46448 0.207377 9.30474L0.207318 9.30458C0.144653 9.1427 0.133055 8.96551 0.174083 8.79684C0.215111 8.62817 0.306799 8.4761 0.436819 8.36108L0.437088 8.36085L9.43643 0.361424C9.43653 0.36134 9.43662 0.361256 9.43672 0.361172C9.59179 0.22506 9.79109 0.15 9.99743 0.15C10.2038 0.15 10.4031 0.225061 10.5581 0.361174C10.5582 0.361258 10.5583 0.361341 10.5584 0.361424L19.5578 8.36085L19.6574 8.24873ZM10.9974 18.1487H11.1474V17.9987V14.9987C11.1474 14.6937 11.0263 14.4012 10.8106 14.1856C10.5949 13.9699 10.3024 13.8487 9.99743 13.8487C9.69244 13.8487 9.39993 13.9699 9.18426 14.1856C8.96859 14.4012 8.84743 14.6937 8.84743 14.9987V17.9987V18.1487H8.99743H10.9974ZM15.9974 18.1487H16.1474V17.9987V9.99873V9.84873H15.9974H3.99743H3.84743V9.99873V17.9987V18.1487H3.99743H6.99743H7.14743V17.9987V14.9987C7.14743 14.2429 7.4477 13.518 7.98218 12.9835C8.51666 12.449 9.24157 12.1487 9.99743 12.1487C10.7533 12.1487 11.4782 12.449 12.0127 12.9835C12.5472 13.518 12.8474 14.2429 12.8474 14.9987V17.9987V18.1487H12.9974H15.9974ZM3.5278 7.8866L3.23279 8.14873H3.62743H16.3674H16.7621L16.4671 7.8866L10.0971 2.2266L9.99743 2.13808L9.8978 2.2266L3.5278 7.8866Z" fill="#737B9C" stroke="#171B27" stroke-width="0.3"/></svg>';
        const svgArrowBack = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="7" height="12" viewBox="0 0 7 12" fill="none"><path d="M0.19006 5.53176L5.49242 0.191706C5.61505 0.068097 5.77876 0 5.95332 0C6.12788 0 6.29159 0.068097 6.41423 0.191706L6.80471 0.584873C7.0588 0.841066 7.0588 1.25745 6.80471 1.51326L2.35219 5.99751L6.80965 10.4867C6.93229 10.6104 7 10.7751 7 10.9508C7 11.1267 6.93229 11.2915 6.80965 11.4152L6.41917 11.8083C6.29644 11.9319 6.13282 12 5.95826 12C5.7837 12 5.61999 11.9319 5.49736 11.8083L0.19006 6.46336C0.067132 6.33936 -0.000385761 6.1738 1.43051e-06 5.99781C-0.000385761 5.82112 0.067132 5.65566 0.19006 5.53176Z" fill="#319DFE"/></svg>';
        const svgLogotype = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="29" height="31" viewBox="0 0 29 31" fill="none"><path d="M26.0528 17.3725V12.5938C26.0528 11.9249 25.5251 11.3828 24.874 11.3828C23.8989 11.3828 23.1057 10.568 23.1057 9.56641C23.1057 8.56478 23.8989 7.75 24.874 7.75C25.8491 7.75 26.6423 8.56478 26.6423 9.56641C26.6423 10.2353 27.17 10.7773 27.8211 10.7773C28.4723 10.7773 29 10.2353 29 9.56641C29 7.22944 27.149 5.32812 24.874 5.32812C23.6686 5.32812 22.5823 5.86193 21.8271 6.71172L18.577 4.89105C18.6092 4.67819 18.626 4.46037 18.626 4.23828C18.626 1.90131 16.7751 0 14.5 0C12.2249 0 10.374 1.90131 10.374 4.23828C10.374 4.45942 10.3906 4.67654 10.4226 4.88845L7.16412 6.70226C6.40937 5.85838 5.32698 5.32812 4.12602 5.32812C1.85095 5.32812 0 7.22944 0 9.56641C0 11.4382 1.18761 13.0292 2.82927 13.589V17.411C1.18761 17.9708 0 19.5618 0 21.4336C0 23.7706 1.85095 25.6719 4.12602 25.6719C5.32537 25.6719 6.40661 25.143 7.16112 24.3011L10.4196 26.134C10.3899 26.3388 10.374 26.5484 10.374 26.7617C10.374 29.0987 12.2249 31 14.5 31C15.1511 31 15.6789 30.4579 15.6789 29.7891C15.6789 29.1202 15.1511 28.5781 14.5 28.5781C13.5249 28.5781 12.7317 27.7633 12.7317 26.7617C12.7317 25.7601 13.5249 24.9453 14.5 24.9453C15.4751 24.9453 16.2683 25.7601 16.2683 26.7617C16.2683 27.4306 16.796 27.9727 17.4472 27.9727C18.0983 27.9727 18.626 27.4306 18.626 26.7617C18.626 26.5368 18.6085 26.3159 18.5754 26.1002L21.8002 24.2571C22.5561 25.1246 23.6539 25.6719 24.874 25.6719C25.5251 25.6719 26.0528 25.1298 26.0528 24.4609C26.0528 23.7921 25.5251 23.25 24.874 23.25C23.8989 23.25 23.1057 22.4352 23.1057 21.4336C23.1057 20.432 23.8989 19.6172 24.874 19.6172C25.8491 19.6172 26.6423 20.432 26.6423 21.4336C26.6423 22.1024 27.17 22.6445 27.8211 22.6445C28.4723 22.6445 29 22.1024 29 21.4336C29 19.5174 27.7553 17.8949 26.0528 17.3725ZM20.8002 8.89495C20.7662 9.11372 20.748 9.33794 20.748 9.56641C20.748 9.92046 20.791 10.2644 20.8711 10.5933L15.6789 13.4253V8.29942C16.411 8.07473 17.0587 7.64712 17.5588 7.07949L20.8002 8.89495ZM6.84085 18.2461C6.37115 17.8232 5.80774 17.508 5.18699 17.3382V13.6621C5.84458 13.4821 6.43792 13.1391 6.92351 12.6782L11.9864 15.4395L6.84085 18.2461ZM14.5 2.42188C15.4751 2.42188 16.2683 3.23666 16.2683 4.23828C16.2683 5.23991 15.4751 6.05469 14.5 6.05469C13.5249 6.05469 12.7317 5.23991 12.7317 4.23828C12.7317 3.23666 13.5249 2.42188 14.5 2.42188ZM11.4393 7.07713C11.9397 7.64594 12.588 8.07426 13.3211 8.29942V13.4253L8.12885 10.5933C8.20898 10.2644 8.25203 9.92046 8.25203 9.56641C8.25203 9.33321 8.23292 9.10474 8.19746 8.88171L11.4393 7.07713ZM4.12602 7.75C5.10111 7.75 5.89431 8.56478 5.89431 9.56641C5.89431 10.568 5.10111 11.3828 4.12602 11.3828C3.15092 11.3828 2.35772 10.568 2.35772 9.56641C2.35772 8.56478 3.15092 7.75 4.12602 7.75ZM4.12602 23.25C3.15092 23.25 2.35772 22.4352 2.35772 21.4336C2.35772 20.432 3.15092 19.6172 4.12602 19.6172C5.10111 19.6172 5.89431 20.432 5.89431 21.4336C5.89431 22.4352 5.10111 23.25 4.12602 23.25ZM11.426 23.9387L8.19677 22.1223C8.23269 21.8981 8.25203 21.6682 8.25203 21.4336C8.25203 21.0412 8.19885 20.6616 8.10122 20.3007L13.3211 17.4536V22.7006C12.5814 22.9276 11.9281 23.3621 11.426 23.9387ZM17.5526 23.9132C17.0534 23.3491 16.4078 22.9243 15.6789 22.7006V17.4536L20.8988 20.3007C20.8012 20.6616 20.748 21.0412 20.748 21.4336C20.748 21.6467 20.7639 21.856 20.7933 22.0606L17.5526 23.9132ZM22.1591 18.2461L17.0136 15.4395L22.0765 12.6779C22.5328 13.1115 23.0845 13.44 23.6951 13.6275V17.3725C23.1209 17.5487 22.5991 17.8502 22.1591 18.2461Z" fill="#319DFE"/></svg>';
        const svgMenu = '<svg width="25" height="17" viewBox="0 0 25 17" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="4" y="0.5" width="17" height="2" rx="1" fill="#737B9C"/><rect y="7.5" width="25" height="2" rx="1" fill="#319DFE"/><rect x="4" y="14.5" width="17" height="2" rx="1" fill="#737B9C"/></svg>';
        const svgClose = '<svg width="14" height="14" viewBox="0 0 14 14" fill="none"' +
            ' xmlns="http://www.w3.org/2000/svg"><rect x="1.41406" y="0.00390625" width="17" height="2" rx="1" transform="rotate(45 1.41406 0.00390625)" fill="#737B9C"/><rect x="0.217285" y="12.0215" width="17" height="2" rx="1" transform="rotate(-45 0.217285 12.0215)" fill="#737B9C"/></svg>';
        const svgBell = '<svg width="18" height="24" viewBox="0 0 18 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.8318 15.4442V10.0247C15.8318 7.02097 13.8282 4.48105 11.1001 3.69712V2.62143C11.1001 1.45167 10.158 0.5 8.99996 0.5C7.84195 0.5 6.89985 1.45167 6.89985 2.62143V3.69717C4.17179 4.48105 2.1681 7.02097 2.1681 10.0247V15.4442C0.950238 15.5812 0 16.627 0 17.8931V18.0878C0 19.4468 1.0945 20.5525 2.43981 20.5525H6.31267V20.7854C6.31267 22.2822 7.51821 23.5 9 23.5C10.4818 23.5 11.6873 22.2822 11.6873 20.7854V20.5525H15.5601C16.9055 20.5525 18 19.4468 18 18.0878V17.8931C17.9999 16.627 17.0496 15.5812 15.8318 15.4442ZM8.2815 2.62143C8.2815 2.22122 8.60382 1.89563 9 1.89563C9.39618 1.89563 9.7185 2.22122 9.7185 2.62143V3.46016C9.58687 3.45212 9.45431 3.44749 9.32072 3.44749H8.67924C8.54565 3.44749 8.41308 3.45207 8.2815 3.46012V2.62143ZM3.5497 10.0247C3.5497 7.16755 5.85076 4.84312 8.67919 4.84312H9.32067C12.1491 4.84312 14.4502 7.16755 14.4502 10.0247V15.4286H3.5497V10.0247ZM8.99996 22.1043C8.27998 22.1043 7.69422 21.5126 7.69422 20.7853V20.5524H10.3056V20.7853H10.3057C10.3057 21.5127 9.71993 22.1043 8.99996 22.1043ZM16.6184 18.0878H16.6183C16.6183 18.6772 16.1435 19.1568 15.5601 19.1568H2.43981C1.85632 19.1568 1.3816 18.6772 1.3816 18.0878V17.8931C1.3816 17.3036 1.85632 16.8241 2.43981 16.8241H2.85894H15.141H15.5601C16.1436 16.8241 16.6184 17.3036 16.6184 17.8931V18.0878Z" fill="#737B9C"/></svg>';
        let {badge} = this.state;
        return (
            <View style={[{flex: 1, minHeight: 70, maxHeight: 70}]}>
                <View style={{backgroundColor: '#121520', flex: 1, flexDirection: 'row', minHeight: 70, maxHeight: 70, alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{minWidth: '25%', maxWidth: '25%', flex: 1}}>
                        {(this.sub) ?
                            <TouchableOpacity style={{flex: 1, flexDirection: 'row', alignItems: 'center', paddingLeft: 10}} onPress={() => this.navigation.goBack()}>
                                <SvgXml xml={svgArrowBack} width="7" height="12" />
                                <Text style={{color: '#737B9C', fontFamily: 'DINNextLTPro', fontSize: normalize(18), lineHeight: 73, marginLeft: 10}}>Back</Text>
                            </TouchableOpacity> :
                            (this.route.name != 'Home') ?
                            <TouchableOpacity onPress={() => this.navigation.navigate('Home')}><SvgXml xml={svgHome} style={{marginLeft: 15}}/></TouchableOpacity>
                            :
                            this.state.loading_badge && this.state.badge != 0 ?
                                <TouchableOpacity onPress={() => this.navigation.navigate('News', {timevalue: JSON.stringify(new Date())})}>
                                    <SvgXml xml={svgBell} width="18" height="21" style={{marginLeft: 15}} />
                                        <View style={styles.badge}>
                                            <Text style={styles.badge_text}>{badge}</Text>
                                        </View>
                                </TouchableOpacity>
                                :
                                <SvgXml xml={svgBell} width="18" height="21" style={{marginLeft: 15}} />

                        }
                    </View>
                    <View style={{minWidth: '50%', maxWidth: '50%', flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <View style={{minWidth: 29, minHeight: 31}}>
                            <SvgXml width="29" height="31" xml={svgLogotype} style={{marginRight: 10}}/>
                        </View>
                        <Text style={{color: "#EAECF2", fontFamily: 'Trenda-Black', fontSize: normalize(19), lineHeight: 70}}>{'Tezos Nodes'.toUpperCase()}</Text>
                    </View>
                    <View style={{minWidth: '25%', maxWidth: '25%', flex: 1, alignItems: 'flex-end', justifyContent: 'center'}}>
                        {this.route.name != 'Menu' ?
                            <TouchableOpacity onPress={() => this.navigation.navigate('Menu')}>
                                <SvgXml xml={svgMenu} style={[{padding: 10, marginRight: 15}]}/>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={() => this.navigation.goBack()}>
                                <SvgXml xml={svgClose} style={{padding: 7, marginRight: 20}} />
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: normalize(22),
        fontFamily: 'Circe',
        color: '#EAECF2'
    },
    badge: {
        position: 'absolute',
        top: 1,
        left: 26,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: 13,
        height: 13,
        padding: 0,
        margin: 0,
        borderRadius: 20,
        backgroundColor: '#319DFE',
    },
    badge_text: {
        fontSize: normalize(10),
        fontFamily: 'Circe_bold',
        color: '#ffffff',
        lineHeight: 15
    }
})