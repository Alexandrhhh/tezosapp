import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    Platform
} from 'react-native';
import PropTypes from 'prop-types';

import {SvgXml} from 'react-native-svg';
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler';

// Icon
import Feather from 'react-native-vector-icons/Feather';

class DropDownPicker extends React.Component {
    constructor(props) {
        super(props);

        let choice;

        if (props.defaultNull || (props.hasOwnProperty('defaultValue') && props.defaultValue === null)) {
            choice = this.null();
        } else if (props.defaultValue) {
            choice = props.items.find(item => item.value === props.defaultValue);
        } else if (props.items.filter(item => item.hasOwnProperty('selected') && item.selected === true).length > 0) {
            choice = props.items.filter(item => item.hasOwnProperty('selected') && item.selected === true)[0];
        } else if (props.items.length > 0) {
            choice = props.items[props.defaultIndex ?? 0];
        } else {
            choice = this.null();
        }

        this.state = {
            choice: {
                label: choice.label,
                value: choice.value,
                svg: choice.svg
            },
            visible: false,
        };
    }

    static getDerivedStateFromProps(props, state) {
        if (props.defaultNull === true) {
            return {
                choice: {
                    label: null,
                    value: null,
                    svg: null
                },
                visible: props.disabled ? false : state.visible,
            };
        }

        return null;
    }

    null() {
        return {
            label: null,
            value: null,
            svg: null
        }
    }

    toggle() {
        this.setState({
            visible: ! this.state.visible
        });
    }

    select(item, index) {
        this.setState({
            choice: {
                label: item.label,
                value: item.value,
                svg: item.svg
            },
            visible: false
        });

        this.props.defaultNull = false;

        // onChangeItem callback
        this.props.onChangeItem(item, index);
    }

    getLayout(layout) {
        this.setState({
            top: layout.height - 1
        });
    }

    render() {
        const { defaultNull, placeholder, disabled } = this.props;
        const isPlaceholderActive = (defaultNull) && this.state.choice.label === null;
        const label = isPlaceholderActive ? (placeholder) : this.state.choice.label;
        const svg = this.state.choice.svg;
        const placeholderStyle = isPlaceholderActive && this.props.placeholderStyle;
        const opacity = disabled ? 0.5 : 1;
        const svgAsc = '<svg width="23" height="14" viewBox="0 0 23 14" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="11" y="1" width="12" height="2" rx="1" fill="#737B9C"/><rect x="11" y="6" width="9" height="2" rx="1" fill="#737B9C"/><rect x="11" y="11" width="7" height="2" rx="1" fill="#737B9C"/><path d="M5.38223 12.8366L8.8417 9.26556C9.05277 9.04769 9.05277 8.69434 8.8417 8.47646C8.63058 8.25854 8.28836 8.25854 8.07724 8.47646L5.54055 11.0949V1.55797C5.54055 1.24984 5.29851 1 5.00001 1C4.70154 1 4.45946 1.24984 4.45946 1.55797L4.45946 11.0949L1.92277 8.47655C1.71165 8.25863 1.36943 8.25863 1.15831 8.47655C1.05284 8.58547 1 8.72831 1 8.8711C1 9.0139 1.05284 9.15669 1.15831 9.26565L4.61778 12.8366C4.82889 13.0545 5.17112 13.0545 5.38223 12.8366Z" fill="#737B9C" stroke="#737B9C" stroke-width="0.6"/></svg>';
        const svgDesc = '<svg width="23" height="14" viewBox="0 0 23 14" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="11" y="1" width="12" height="2" rx="1" fill="#737B9C"/><rect x="11" y="6" width="9" height="2" rx="1" fill="#737B9C"/><rect x="11" y="11" width="7" height="2" rx="1" fill="#737B9C"/><path d="M4.61777 1.16344L1.1583 4.73444C0.947232 4.95231 0.947232 5.30566 1.1583 5.52354C1.36942 5.74146 1.71164 5.74146 1.92276 5.52354L4.45945 2.90506V12.442C4.45945 12.7502 4.70149 13 4.99999 13C5.29846 13 5.54054 12.7502 5.54054 12.442L5.54054 2.90506L8.07723 5.52345C8.28835 5.74137 8.63057 5.74137 8.84169 5.52345C8.94716 5.41453 9 5.27169 9 5.1289C9 4.9861 8.94716 4.84331 8.84169 4.73435L5.38222 1.16344C5.17111 0.945519 4.82888 0.945519 4.61777 1.16344Z" fill="#737B9C" stroke="#737B9C" stroke-width="0.6"/></svg>';

        return (
            <View style={[this.props.containerStyle, {
                ...Platform.select({
                    ios: {
                      zIndex: this.props.zIndex
                    }
                })
            }]}>
                <TouchableOpacity onLayout={(event) => { this.getLayout(event.nativeEvent.layout) }} disabled={disabled} onPress={() => this.toggle()} activeOpacity={1} style={[styles.dropDown, this.props.style, this.state.visible && styles.noBottomRadius, {flexDirection: 'row', flex: 1}]}>
                    <View style={[styles.dropDownDisplay, this.props.activeDropItemStyle]}>
                        <SvgXml xml={svgAsc} style={svg == 'desc' ? {display:'none'} : ''}  />
                        <SvgXml xml={svgDesc} style={svg == 'asc' ? {display:'none'} : ''}/>
                        <Text style={[this.props.labelStyle, placeholderStyle, {opacity}]}>{label}</Text>
                    </View>
                    {this.props.showArrow && (
                        <View style={[styles.arrow]}>
                            <View style={[this.props.arrowStyle, {opacity}]}>
                            {
                                ! this.state.visible ? (
                                    this.props.customArrowUp ?? <Feather name="chevron-down" size={this.props.arrowSize} color={this.props.arrowColor} />
                                ) : (
                                    this.props.customArrowDown ?? <Feather name="chevron-up" size={this.props.arrowSize} color={this.props.arrowColor} />
                                )
                            }
                            </View>
                        </View>
                    )}
                </TouchableOpacity>
                <View style={[styles.dropDown, styles.dropDownBox, this.props.dropDownStyle, ! this.state.visible && styles.hidden, {
                    top: this.state.top,
                    maxHeight: this.props.dropDownMaxHeight,
                    zIndex: this.props.zIndex
                }]}>
                    <GestureHandlerScrollView style={{width: '100%', zIndex: this.props.zIndex}} nestedScrollEnabled={true}>
                        {
                            this.props.items.map((item, index) => (
                                <TouchableOpacity key={index} onPress={() => this.select(item, index)} style={[styles.dropDownItem, this.props.itemStyle, (
                                    this.state.choice.value === item.value && this.props.activeItemStyle
                                )]}>
                                    <SvgXml xml={svgAsc} style={item.svg == 'desc' ? {display:'none'} : ''}  />
                                    <SvgXml xml={svgDesc} style={item.svg == 'asc' ? {display:'none'} : ''}/>
                                    <Text style={[this.props.labelStyle,
                                        this.state.choice.value === item.value && this.props.activeLabelStyle
                                    ]}>{item.label}</Text>
                                </TouchableOpacity>
                            ))
                        }
                    </GestureHandlerScrollView>
                </View>
            </View>
        );
    }
}

DropDownPicker.defaultProps = {
    defaultNull: false,
    placeholder: 'Select an item',
    dropDownMaxHeight: 150,
    style: {},
    dropDownStyle: {},
    containerStyle: {},
    itemStyle: {},
    labelStyle: {},
    placeholderStyle: {},
    activeItemStyle: {},
    activeDropItemStyle: {},
    activeLabelStyle: {},
    arrowStyle: {},
    arrowColor: '#000',
    showArrow: true,
    arrowSize: 15,
    customArrowUp: null,
    customArrowDown: null,
    zIndex: 5000,
    disabled: false,
    onChangeItem: () => {},
};

DropDownPicker.propTypes = {
    items: PropTypes.array.isRequired,
    defaultIndex: PropTypes.number,
    defaultValue: PropTypes.any,
    defaultNull: PropTypes.bool,
    placeholder: PropTypes.string,
    dropDownMaxHeight: PropTypes.number,
    style: PropTypes.object,
    dropDownStyle: PropTypes.object,
    activeDropItemStyle: PropTypes.object,
    containerStyle: PropTypes.object,
    itemStyle: PropTypes.object,
    labelStyle: PropTypes.object,
    activeItemStyle: PropTypes.object,
    activeLabelStyle: PropTypes.object,
    showArrow: PropTypes.bool,
    arrowStyle: PropTypes.object,
    arrowColor: PropTypes.string,
    arrowSize: PropTypes.number,
    customArrowUp: PropTypes.any,
    customArrowDown: PropTypes.any,
    zIndex: PropTypes.number,
    disabled: PropTypes.bool,
    onChangeItem: PropTypes.func
};

const styles = StyleSheet.create({
    arrow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        paddingVertical: 8,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
    },
    dropDown: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: '#fff',
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderWidth: 1,
        borderColor: '#dfdfdf',
    },
    dropDownDisplay: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
        flexGrow: 1
    },
    dropDownBox: {
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        position: 'absolute',
        width: '100%'
    },
    dropDownItem: {
        paddingVertical: 8,
        width: '100%',
        justifyContent: 'center'
    },
    hidden: {
        position: 'relative',
        display: 'none'
    },
    noBottomRadius: {
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
    }
});

export default DropDownPicker;