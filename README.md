Tezos Nodes is a reliability rating public Tezos Bakers and a service for monitoring the state of node performance for non-public Tezos Bakers.

Working with the baking community, we've developed a baker reliability index that features 10 key indicators. We think this index will provide you with all the necessary info you need to choose a reliable public Baker.

The Tezos Nodes app has many features and important data provide such as:

• Reliability rating Tezos Bakers
• Tezos network data
• News about Tezos project  
• The Rewards calculator
• In personal account Bakers will be able to manage important data. All users will be able to get information about the efficiency and free space for delegation about any node in the Tezos network.
• The app also contains a list of inactive Bakers who have closed their bakery service. If you still delegate any of them, please immediately change the Baker.

Support and feedback:

If you have any issues using our application or would like to send feedback, send us an email at support@tezos-nodes.com or contact us using the application, namely support tab in the menu.

This application was created with the support of Tezos Foundation https://tezos.foundation

Designed by Expo Platform https://expo.io